package com.app_2udezz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.app_2udezz.Activities.CameraPreviewFragment;
import com.app_2udezz.Activities.LoginActivity;
import com.app_2udezz.Activities.PostDetailActivity;
import com.app_2udezz.Fragments.AddFragment;
import com.app_2udezz.Fragments.ChatFragment;
import com.app_2udezz.Fragments.HomeFragment;
import com.app_2udezz.Fragments.NotificationFragment;
import com.app_2udezz.Fragments.ProfileFragment;
import com.app_2udezz.Fragments.SearchFragment;
import com.app_2udezz.Util.SavePref;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    private SavePref savePref;
    @BindView(R.id.title)
    TextView title;
    public static Toolbar toolbar;
    public static MainActivity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        context = MainActivity.this;
        savePref = new SavePref(context);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (getIntent().getBooleanExtra("from_add", false)) {
            navigation.setSelectedItemId(R.id.navigation_profile);
        } else if (getIntent().getBooleanExtra("is_from_push", false)) {
            navigation.setSelectedItemId(R.id.navigation_home);
        } else if (getIntent().getBooleanExtra("from_add_story", false)) {
            navigation.setSelectedItemId(R.id.navigation_home);
        } else {
            navigation.setSelectedItemId(R.id.navigation_add);
        }


    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            toolbar.setVisibility(View.GONE);
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    toolbar.setVisibility(View.GONE);
                    loadFragment(new HomeFragment());
                    return true;
                /*case R.id.navigation_search:
                    title.setText("Search");
                    loadFragment(new SearchFragment());
                    return true;*/
                case R.id.navigation_chat:
                    title.setText("Chat");
                    loadFragment(new ChatFragment());
                    return true;
                case R.id.navigation_add:
                    title.setText("Add Post");
                    loadFragment(new CameraPreviewFragment());
                    return true;
                case R.id.navigation_notification:
                    title.setText("Notifications");
                    loadFragment(new NotificationFragment());
                    return true;
                case R.id.navigation_profile:
                    toolbar.setVisibility(View.GONE);
                    loadFragment(new ProfileFragment());
                    return true;
            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        shareopencheck();
    }

    private void shareopencheck() {
        try {
            //check_notification_flag = true;
            final Intent intent = getIntent();
            final String action = intent.getAction();
            Uri data = intent.getData();
            Log.e("data_share", data.toString());
            String post_id = data.toString().substring(data.toString().lastIndexOf("/") + 1);
            Log.e("post_id", post_id);
            //check_link = true;
            SavePref savePref = new SavePref(context);
            // savePref.setPoll_id(pollid);
            if (savePref.getAuthorization_key().isEmpty()) {
                //util.link_loginopen = true;
                Intent intent2 = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent2);
                finish();
            } else {
                Intent intent1 = new Intent(context, PostDetailActivity.class);
                intent1.putExtra("is_from_push", true);
                intent1.putExtra("post_id", post_id);
                context.startActivity(intent1);
            }

        } catch (Exception e) {

        }
    }

}
