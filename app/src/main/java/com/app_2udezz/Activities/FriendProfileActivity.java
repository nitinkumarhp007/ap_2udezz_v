package com.app_2udezz.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Adapters.FriendPostsAdapter;
import com.app_2udezz.DataModels.PostModel;
import com.app_2udezz.MainActivity;
import com.app_2udezz.R;
import com.app_2udezz.Util.ConnectivityReceiver;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;
import com.app_2udezz.parser.GetAsyncDELETE;
import com.app_2udezz.parser.GetAsyncGet;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FriendProfileActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener,
        MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnPreparedListener {
    FriendProfileActivity context;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email_address)
    TextView emailAddress;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.followers)
    TextView followers;
    @BindView(R.id.followers_layout)
    LinearLayout followersLayout;
    @BindView(R.id.following)
    TextView following;
    @BindView(R.id.following_layout)
    LinearLayout followingLayout;
    @BindView(R.id.follow_unfollow)
    TextView followUnfollow;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.scroll_bar)
    ScrollView scrollBar;
    @BindView(R.id.error_text)
    TextView errorText;
    @BindView(R.id.navigation_chat)
    TextView navigationChat;
    @BindView(R.id.audio_play)
    ImageView audioPlay;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.option)
    ImageView option;
    @BindView(R.id.likes)
    TextView likes;
    @BindView(R.id.likes_layout)
    LinearLayout likesLayout;
    private SavePref savePref;
    String friend_id = "";
    FriendPostsAdapter adapter = null;
    private ArrayList<PostModel> list;
    private MediaPlayer mediaPlayer;
    String is_follow = "";
    String selectedaudio = "";

    boolean is_from_push = false;

    boolean is_play = false;
    boolean is_load = false;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_friend_profile);
        ButterKnife.bind(this);


        init();
    }

    private void init() {
        context = FriendProfileActivity.this;
        savePref = new SavePref(context);
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnBufferingUpdateListener(this);
        mediaPlayer.setOnCompletionListener(this);

        /*mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                audioPlay.setImageDrawable(getResources().getDrawable(R.drawable.play_));
                if (mediaPlayer != null) {
                    mediaPlayer.stop();
                    mediaPlayer.prepareAsync();
                }
            }
        });*/

        progressDialog = util.initializeProgress(context);


        friend_id = getIntent().getStringExtra("user_id");
        is_from_push = getIntent().getBooleanExtra("is_from_push", false);


        if (ConnectivityReceiver.isConnected()) {
            USER_DETAIL_API();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }


    private void USER_DETAIL_API() {

        if (friend_id.contains("@"))
            friend_id = friend_id.substring(1);

        progressDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.POST_ID, "200");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.USER_DETAIL + "/" + friend_id, formBody) {
            @Override
            public void getValueParse(String result) {
                progressDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject data = jsonmainObject.getJSONObject("data");

                            JSONObject user_info = data.getJSONObject("user_info");
                            name.setText(user_info.getString("first_name"));
                            emailAddress.setText(user_info.getString("about_us"));
                            //   phone.setText(user_info.getString("phone"));
                            Glide.with(context).load(user_info.getString("profile")).into(profilePic);

                            followers.setText(data.getJSONObject("otherinfo").getString("total_follower"));
                            following.setText(data.getJSONObject("otherinfo").getString("total_following"));
                            likes.setText(data.optString("total_likes"));
                            selectedaudio = data.getJSONObject("user_info").getString("audio");

                            if (user_info.getString("verfiy_badge").equals("1")) {
                                name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.verified, 0);

                            } else {
                                name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            }

                            if (friend_id.equals(savePref.getID())) {
                                followUnfollow.setText("Edit Profile");
                                navigationChat.setVisibility(View.GONE);
                            } else {
                                is_follow = user_info.getString("i_follow");

                                Log.e("is_follow_value", is_follow);
                                if (is_follow.equals("1")) {
                                    followUnfollow.setText("Unfollow");
                                    navigationChat.setVisibility(View.VISIBLE);
                                } else {
                                    followUnfollow.setText("Follow");
                                    navigationChat.setVisibility(View.GONE);
                                }
                            }
                            scrollBar.setVisibility(View.VISIBLE);

                            if (user_info.getString("is_private").equals("1") && is_follow.equals("0")) {
                                myRecyclerView.setVisibility(View.GONE);
                                errorText.setVisibility(View.VISIBLE);
                                errorText.setText("This account is private");
                            } else {
                                JSONArray body = data.getJSONArray("posts");
                                for (int i = 0; i < body.length(); i++) {
                                    JSONObject object = body.getJSONObject(i);
                                    PostModel postModel = new PostModel();
                                    postModel.setCreated(object.getString("created"));
                                    postModel.setDescription(object.getString("description"));
                                    postModel.setId(object.getString("id"));
                                    postModel.setIs_like(object.getString("is_like"));
                                    postModel.setLocations(object.getString("locations"));
                                    postModel.setMedia(object.getString("media"));
                                    postModel.setPost_type(object.getString("post_type"));
                                    postModel.setTitle(object.getString("title"));
                                    postModel.setTotal_comments(object.getString("total_comments"));
                                    postModel.setTotal_likes(object.getString("total_likes"));
                                    postModel.setUser_id(object.getString("user_id"));
                                    postModel.setVerfiy_badge(object.optString("verfiy_badge"));
                                    postModel.setImage(user_info.getString("profile"));
                                    if (friend_id.equals(savePref.getID())) {
                                        postModel.setIs_show_delete(true);
                                    } else {
                                        postModel.setIs_show_delete(false);
                                    }

                                    postModel.setName(user_info.getString("first_name"));
                                    postModel.setMedia_type(object.optString("media_type"));

                                    list.add(postModel);
                                }

                                if (list.size() > 0) {
                                    adapter = new FriendPostsAdapter(context, list);
                                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                    myRecyclerView.setAdapter(adapter);
                                    myRecyclerView.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);
                                } else {
                                    errorText.setVisibility(View.VISIBLE);
                                    myRecyclerView.setVisibility(View.GONE);
                                }

                            }


                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        Toast.makeText(FriendProfileActivity.this, ex.toString(), Toast.LENGTH_SHORT).show();
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        Toast.makeText(FriendProfileActivity.this, ex.toString(), Toast.LENGTH_SHORT).show();
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void DeleteAlert(int position) {
        new IOSDialog.Builder(context)
                .setCancelable(false)
                .setMessage("Are you sure to Delete Post?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DELETE_THREAD_API(position);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void DELETE_THREAD_API(int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.POST_ID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncDELETE mAsync = new GetAsyncDELETE(context, AllAPIS.DELETE_POST + "?post_id=" + list.get(position).getId(), formBody, mDialog, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(Response response) {
                mDialog.dismiss();

                String result = response.body().toString();
                if (response.isSuccessful()) {
                    new IOSDialog.Builder(context)
                            .setCancelable(false)
                            .setMessage("Post Deleted Successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            list.remove(position);
                            adapter.notifyDataSetChanged();
                        }
                    }).show();
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void LIKE_DISLIKE_POST(int position) {
        String api_url = "";
        if (list.get(position).getIs_like().equals("1")) {
            api_url = AllAPIS.POST_DISLIKE;
        } else {
            api_url = AllAPIS.POST_LIKE;
        }

        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.POST_ID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, api_url, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, jsonMainobject.getString("message"));
                            if (list.get(position).getIs_like().equals("0")) {
                                list.get(position).setIs_like("1");
                                list.get(position).setTotal_likes(String.valueOf(Integer.parseInt(list.get(position).getTotal_likes()) + 1));
                                likes.setText(String.valueOf(Integer.parseInt(likes.getText().toString()) + 1));
                            } else {
                                list.get(position).setIs_like("0");
                                likes.setText(String.valueOf(Integer.parseInt(likes.getText().toString()) - 1));
                                list.get(position).setTotal_likes(String.valueOf(Integer.parseInt(list.get(position).getTotal_likes()) - 1));
                            }
                            adapter.notifyDataSetChanged();

                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onBackPressed() {
        if (is_from_push) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("is_from_push", true);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null)
            mediaPlayer.stop();
    }

    @OnClick({R.id.back_button, R.id.option, R.id.audio_play, R.id.navigation_chat, R.id.followers_layout, R.id.following_layout, R.id.follow_unfollow})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.audio_play:
                if (selectedaudio != null) {
                    if (!selectedaudio.isEmpty()) {
                        progressDialog.show();

                        if (!is_play) {
                            progressBar.setVisibility(View.VISIBLE);
                            if (selectedaudio != null) {
                                if (!selectedaudio.isEmpty()) {
                                    try {
                                        if (!is_load) {
                                            is_load = true;
                                            Log.e("selectedaudio__", selectedaudio);
                                            mediaPlayer.setDataSource(selectedaudio);

                                            mediaPlayer.prepareAsync();
                                            /*audioPlay.setImageDrawable(getResources().getDrawable(R.drawable.pause_));
                                            if (mediaPlayer != null) {
                                                mediaPlayer.start();
                                            }*/

                                        } else {
                                            progressBar.setVisibility(View.INVISIBLE);
                                            if (mediaPlayer != null) {
                                                audioPlay.setImageDrawable(getResources().getDrawable(R.drawable.pause_));
                                                mediaPlayer.start();
                                            }
                                        }
                                    } catch (Exception e) {
                                        progressBar.setVisibility(View.INVISIBLE);
                                        is_load = false;
                                        audioPlay.setImageDrawable(getResources().getDrawable(R.drawable.play_));
                                        //Log.e("Error", e.getMessage());
                                    }
                                }
                            }

                            /*audioPlay.setImageDrawable(getResources().getDrawable(R.drawable.pause_));
                            if (mediaPlayer != null) {
                                mediaPlayer.start();
                            }*/
                            //progressBar.setVisibility(View.INVISIBLE);
                            is_play = true;
                        } else {
                            if (selectedaudio != null) {
                                if (!selectedaudio.isEmpty()) {
                                    audioPlay.setImageDrawable(getResources().getDrawable(R.drawable.play_));
                                    if (mediaPlayer != null)
                                        mediaPlayer.pause();
                                    is_play = false;
                                }
                            }
                        }
                        progressDialog.hide();
                    } else {
                        util.IOSDialog(context, "Audio Not Found");
                    }
                }

                break;
            case R.id.navigation_chat:
                Intent intent = new Intent(context, ChattngActivity.class);
                intent.putExtra("friend_id", friend_id);
                intent.putExtra("friend_name", name.getText().toString().trim());
                startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.followers_layout:
                GOTONEXT("1");
                break;
            case R.id.back_button:
                finish();
                break;
            case R.id.option:
                Show_Option();
                break;
            case R.id.following_layout:
                GOTONEXT("0");
                break;
            case R.id.follow_unfollow:
                if (friend_id.equals(savePref.getID())) {
                    startActivity(new Intent(context, UpdateProfileActivity.class));
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                } else {
                    if (is_follow.equals("1")) {
                        FOLLOW_UNFOLLOW_API(AllAPIS.FOLLOW);
                    } else {
                        FOLLOW_UNFOLLOW_API(AllAPIS.FOLLOW);
                    }


                }
                break;
        }
    }

    private void Show_Option() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        //builder.setTitle("Choose an Category");

        /*ArrayList to Array Conversion */
        String array[] = new String[2];
        array[0] = "Block User";
        array[1] = "Cancel";


        builder.setItems(array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    Alert();
                }
                dialog.dismiss();
            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void Alert() {
        new IOSDialog.Builder(context)
                .setMessage("Are you sure to Block?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        USER_BLOCK();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void USER_BLOCK() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.FRIEND_ID, friend_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USER_BLOCK, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, jsonMainobject.getString("message"));
                            finish();
                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void GOTONEXT(String type) {
        Intent intent = new Intent(context, UsersListActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("from_friend", true);
        intent.putExtra("user_id", savePref.getID());
        intent.putExtra("friend_id", friend_id);
        startActivity(intent);
        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }

    public void FOLLOW_UNFOLLOW_API(String api_url) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.FRIEND_ID, friend_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, api_url, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, jsonMainobject.getString("message"));
                            if (is_follow.equals("1")) {
                                is_follow = "0";
                                followUnfollow.setText("Follow");
                                navigationChat.setVisibility(View.GONE);

                                followers.setText(String.valueOf(Integer.parseInt(followers.getText().toString()) - 1));

                            } else {
                                is_follow = "1";
                                followUnfollow.setText("Unfollow");
                                navigationChat.setVisibility(View.VISIBLE);

                                followers.setText(String.valueOf(Integer.parseInt(followers.getText().toString()) + 1));

                            }
                            USER_DETAIL_API();
                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        progressBar.setVisibility(View.INVISIBLE);
        if (mediaPlayer != null) {
            audioPlay.setImageDrawable(getResources().getDrawable(R.drawable.pause_));
            mediaPlayer.start();
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        /*progressBar.setVisibility(View.INVISIBLE);

        if (mediaPlayer != null) {
            audioPlay.setImageDrawable(getResources().getDrawable(R.drawable.pause_));
            mediaPlayer.start();
        }*/
    }
}
