package com.app_2udezz.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.app_2udezz.Adapters.CommentAdapter;
import com.app_2udezz.Adapters.StoryViewPagerAdapter;
import com.app_2udezz.DataModels.CommentModel;
import com.app_2udezz.DataModels.PostModel;
import com.app_2udezz.DataModels.StoryModel;
import com.app_2udezz.R;
import com.app_2udezz.Util.CubeTransformer;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class StoryViewActivity extends AppCompatActivity {
    StoryViewActivity context;
    private SavePref savePref;
    public ViewPager viewpager;
    private ArrayList<PostModel> post_list;
    private ArrayList<StoryModel> list;
    public int position_item = 0;
    boolean is_friend = false;

    StoryViewPagerAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_view);

        context = StoryViewActivity.this;
        savePref = new SavePref(context);

        viewpager = (ViewPager) findViewById(R.id.viewpager);

        post_list = getIntent().getParcelableArrayListExtra("list");

        position_item = getIntent().getIntExtra("position", 0);
        is_friend = getIntent().getBooleanExtra("is_friend", false);

        adapter = new StoryViewPagerAdapter(this, post_list, position_item);
        viewpager.setPageTransformer(true, new CubeTransformer());
        viewpager.setAdapter(adapter);

        viewpager.setCurrentItem(position_item);


        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int pos) {
                position_item = pos;

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void setNextTask(int position) {
        list = post_list.get(position).getList();
        adapter = new StoryViewPagerAdapter(StoryViewActivity.this, post_list, position);
        viewpager.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        // Very important !
        adapter.storiesProgressView.destroy();
        super.onDestroy();
    }

    public void POST_COMMENT(String comment, String story_id, String user_id) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.COMMENT, comment);
        formBuilder.addFormDataPart(Parameters.STORY_ID, story_id);
        formBuilder.addFormDataPart(Parameters.USER_ID, user_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.STORY_REPLY, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                util.hideKeyboard(context);
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject object = jsonmainObject.getJSONObject("data");
                            util.showToast(context, "Messsage sent");
                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

}