package com.app_2udezz.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app_2udezz.MainActivity;
import com.app_2udezz.R;
import com.app_2udezz.Util.ConnectivityReceiver;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.app_2udezz.Util.util.getDataColumn;
import static com.app_2udezz.Util.util.isDownloadsDocument;
import static com.app_2udezz.Util.util.isExternalStorageDocument;
import static com.app_2udezz.Util.util.isMediaDocument;

public class UpdateProfileActivity extends AppCompatActivity {
    UpdateProfileActivity context;
    @BindView(R.id.profile_pic)
    ImageView profilePic;
    @BindView(R.id.first_name)
    EditText firstName;
    @BindView(R.id.last_name)
    EditText lastName;
    @BindView(R.id.email_address)
    EditText emailAddress;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.next_button)
    ImageView nextButton;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.bio)
    EditText bio;
    @BindView(R.id.video_play)
    ImageView videoPlay;
    @BindView(R.id.image)
    ImageView image;
    private SavePref savePref;

    private String selectedimage = "";
    Uri fileUri;
    String selectedaudio = "";
    String selectedaudio_new = "";
    private MediaPlayer mediaPlayer;
    ProgressDialog mDialog;
    boolean is_load = false;
    boolean is_played = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_update_profile);
        ButterKnife.bind(this);

        context = UpdateProfileActivity.this;
        savePref = new SavePref(context);

        mDialog = util.initializeProgress(context);


        setdata();


        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                is_played = false;
                videoPlay.setImageDrawable(getResources().getDrawable(R.drawable.play_));
                if (mediaPlayer != null)
                    mediaPlayer.pause();

                Intent intent_upload = new Intent();
                intent_upload.setType("audio/*");
                intent_upload.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent_upload, 1);
            }
        });

        videoPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!is_played) {
                    if (selectedaudio != null) {
                        if (!selectedaudio.isEmpty()) {

                            if (!selectedaudio.isEmpty()) {
                                try {
                                    is_played = true;
                                    videoPlay.setImageDrawable(getResources().getDrawable(R.drawable.pause_));
                                    mDialog.show();
                                    if (!is_load) {
                                        is_load = true;
                                        mediaPlayer = new MediaPlayer();
                                        mediaPlayer.setDataSource(selectedaudio);
                                        mediaPlayer.prepare();
                                    }

                                    // lengthOfAudio = mediaPlayer.getDuration();
                                    if (mediaPlayer != null)
                                        mediaPlayer.start();

                                    mDialog.dismiss();


                                } catch (Exception e) {
                                    mDialog.dismiss();
                                }
                            }


                        } else {
                            util.IOSDialog(context, "Audio Not Found");
                        }
                    }
                } else {
                    is_played = false;
                    videoPlay.setImageDrawable(getResources().getDrawable(R.drawable.play_));
                    if (mediaPlayer != null)
                        mediaPlayer.pause();
                }
            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null)
            mediaPlayer.stop();
    }

    private void setdata() {
        Glide.with(context).load(savePref.getImage()).into(profilePic);
        firstName.setText(savePref.getFirstName());
        lastName.setText(savePref.getLastName());
        emailAddress.setText(savePref.getEmail());
        phone.setText(savePref.getPhone());
        bio.setText(savePref.getStringLatest("about_us"));
        selectedaudio = savePref.getStringLatest("audio");
        videoPlay.setImageDrawable(getResources().getDrawable(R.drawable.play_));
        /*Handler handler = new Handler();
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    while (true) {
                        sleep(1000);
                        handler.post(this);



                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    mDialog.dismiss();
                }
            }
        };

        thread.start();*/


    }


    @OnClick({R.id.profile_pic, R.id.next_button, R.id.back_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profile_pic:
                CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            case R.id.next_button:
                EDIT_PROFILEProcess();
                break;
            case R.id.back_button:
                finish();
                break;
        }
    }

    private void EDIT_PROFILEProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (firstName.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Your First Name");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } /*else if (lastName.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Your Last Name");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            }*/ else if (emailAddress.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Email Address");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter a Vaild Email Address");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (phone.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Phone");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (bio.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Bio");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                EDIT_PROFILE_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void EDIT_PROFILE_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.PROFILE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }

        if (!selectedaudio_new.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedaudio_new.endsWith("mp3") ?
                    MediaType.parse("audio/mp3") : MediaType.parse("audio/wav");

            File file = new File(selectedaudio_new);
            formBuilder.addFormDataPart(Parameters.AUDIO, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }

        formBuilder.addFormDataPart(Parameters.FIRST_NAME, firstName.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.LAST_NAME, lastName.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHONE, phone.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.ABOUT_US, bio.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        // formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.EDIT_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("data");
                            savePref.setEmail(body.getString("email"));
                            savePref.setFirstName(body.getString("first_name"));
                            savePref.setLastName(body.getString("last_name"));
                            savePref.setPhone(body.optString("phone_code") + body.getString("phone"));
                            savePref.setImage(body.getString("profile"));
                            savePref.setStringLatest("about_us", body.getString("about_us"));
                            savePref.setStringLatest("audio", body.getString("audio"));
                            //savePref.setStringLatest("phone_code", body.getString("phone_code"));
                            finish();
                            util.showToast(context, "Profile Updated Successfully!");
                        } else {
                            nextButton.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                selectedimage = getAbsolutePath(this, resultUri);

                Glide.with(this).load(selectedimage).into(profilePic);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        } else if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                //the selected audio.
                Uri uri = data.getData();
                try {
                    String uriString = uri.toString();
                    File myFile = new File(uriString);
                    //    String path = myFile.getAbsolutePath();
                    String displayName = null;
                    selectedaudio = getPath(context, uri);
                    selectedaudio_new = selectedaudio;
                    File f = new File(selectedaudio);
                    long fileSizeInBytes = f.length();
                    long fileSizeInKB = fileSizeInBytes / 1024;
                    long fileSizeInMB = fileSizeInKB / 1024;
                    if (fileSizeInMB > 8) {
                        selectedaudio = "";
                        selectedaudio_new = "";
                        // selectedaudio="";
                        util.IOSDialog(context, "Sorry file size is large, Use Audio under 8 MB");
                    } else {
                        is_load = false;
                        videoPlay.setImageDrawable(getResources().getDrawable(R.drawable.play_));

                    }
                } catch (Exception e) {
                    //handle exception
                    Toast.makeText(this, "Unable to process,try again", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    public static String getPath(final Context context, final Uri uri) {
        final boolean isKitKatOrAbove = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKatOrAbove && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video1".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }
}
