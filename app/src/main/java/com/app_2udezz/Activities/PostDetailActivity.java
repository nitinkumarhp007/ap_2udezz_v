package com.app_2udezz.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.app_2udezz.DataModels.PostModel;
import com.app_2udezz.MainActivity;
import com.app_2udezz.R;
import com.app_2udezz.Util.ConnectivityReceiver;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;
import com.app_2udezz.parser.GetAsyncGet;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.luseen.autolinklibrary.AutoLinkOnClickListener;
import com.luseen.autolinklibrary.AutoLinkTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class PostDetailActivity extends AppCompatActivity {
    PostDetailActivity context;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.description)
    AutoLinkTextView description;
    @BindView(R.id.image)
    ImageViewZoom image;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.like)
    TextView like;
    @BindView(R.id.Comment)
    TextView Comment;
    @BindView(R.id.time_date)
    TextView timeDate;

    ArrayList<PostModel> list;
    String verfiy_badge = "";

    String post_id = "";
    String user_id = "";

    boolean is_from_push = false;
    @BindView(R.id.main_layout)
    LinearLayout mainLayout;
    @BindView(R.id.share)
    ImageView share;

    private SavePref savePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_post_detail);
        ButterKnife.bind(this);

        context = PostDetailActivity.this;
        savePref = new SavePref(context);
        post_id = getIntent().getStringExtra("post_id");
        is_from_push = getIntent().getBooleanExtra("is_from_push", false);


        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FriendProfileActivity.class);
                intent.putExtra("user_id", user_id);
                context.startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FriendProfileActivity.class);
                intent.putExtra("user_id", user_id);
                context.startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });


        setToolbar();

        Comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CommentsActivity.class);
                intent.putExtra("post_id", post_id);
                context.startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });



        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LIKE_DISLIKE_POST();
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                util.sharepost(post_id, context);
            }
        });

        description.addAutoLinkMode(AutoLinkMode.MODE_HASHTAG, AutoLinkMode.MODE_MENTION);
        description.setHashtagModeColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        description.setMentionModeColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        description.setAutoLinkOnClickListener(new AutoLinkOnClickListener() {
            @Override
            public void onAutoLinkTextClick(AutoLinkMode autoLinkMode, String matchedText) {

                if (autoLinkMode == AutoLinkMode.MODE_MENTION) {

                    // Toast.makeText(context, "Click on MODE_MENTION" + " " + matchedText.substring(1), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(context, FriendProfileActivity.class);
                    intent.putExtra("user_id", matchedText.substring(1));
                    context.startActivity(intent);

                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);

                } else if (autoLinkMode == AutoLinkMode.MODE_HASHTAG) {
                    Intent intent = new Intent(context, PostListingActivity.class);
                    intent.putExtra("hashtag", matchedText.substring(1));
                    context.startActivity(intent);
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                    //  Toast.makeText(context, "Click on MODE_HASHTAG" + " " + matchedText, Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected()) {
            POST_DETAIL();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void POST_DETAIL() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.POST_DETAIL + "/" + post_id, formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {


                            JSONObject object = jsonmainObject.getJSONObject("data");
                            PostModel postModel = new PostModel();
                            postModel.setCreated(object.getString("created"));
                            postModel.setDescription(object.getString("description"));
                            postModel.setId(object.getString("id"));
                            postModel.setIs_like(object.getString("is_like"));
                            postModel.setLocations(object.getString("locations"));
                            postModel.setMedia(object.getString("media"));
                            postModel.setPost_type(object.getString("post_type"));
                            postModel.setTitle(object.getString("title"));
                            postModel.setTotal_comments(object.getString("total_comments"));
                            postModel.setTotal_likes(object.getString("total_likes"));
                            postModel.setUser_id(object.getString("user_id"));
                            postModel.setVerfiy_badge(object.optString("verfiy_badge"));

                            user_id=object.getString("user_id");


                            postModel.setImage(object.getString("profile"));
                            postModel.setName(object.getString("first_name"));
                            list.add(postModel);

                            mainLayout.setVisibility(View.VISIBLE);

                            timeDate.setText(getDate(Long.parseLong(list.get(0).getCreated())));


                            if (list.get(0).getIs_like().equals("1"))
                                like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.h_r, 0, 0, 0);
                            else
                                like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.h_b, 0, 0, 0);

                            description.setAutoLinkText(list.get(0).getTitle());
                            like.setText(list.get(0).getTotal_likes());
                            Comment.setText(list.get(0).getTotal_comments());
                            location.setText(list.get(0).getLocations());
                            name.setText(list.get(0).getName());
                            if (list.get(0).getVerfiy_badge().equals("1")) {
                                name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.verified, 0);

                            } else {
                                name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            }

                            Glide.with(context).load(list.get(0).getImage()).into(profilePic);

                            Glide.with(context)
                                    .load(list.get(0).getMedia())
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progressBar.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progressBar.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(image);


                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void LIKE_DISLIKE_POST() {
        String api_url = "";
        if (list.get(0).getIs_like().equals("1")) {
            api_url = AllAPIS.POST_DISLIKE;
        } else {
            api_url = AllAPIS.POST_LIKE;
        }

        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.POST_ID, list.get(0).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, api_url, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, jsonMainobject.getString("message"));
                            if (list.get(0).getIs_like().equals("0")) {
                                list.get(0).setIs_like("1");
                                list.get(0).setTotal_likes(String.valueOf(Integer.parseInt(list.get(0).getTotal_likes()) + 1));
                            } else {
                                list.get(0).setIs_like("0");
                                list.get(0).setTotal_likes(String.valueOf(Integer.parseInt(list.get(0).getTotal_likes()) - 1));
                            }
                            if (list.get(0).getIs_like().equals("1"))
                                like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.h_r, 0, 0, 0);
                            else
                                like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.h_b, 0, 0, 0);

                            like.setText(list.get(0).getTotal_likes());

                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                    savePref.clearPreferences();
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                } else {
                                    util.showToast(context, jsonMainobject.getString("error_message"));
                                }
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private String getDate(long time) {
        Date date = new Date(time * 1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy "); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));

        return sdf.format(date);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Post Detail");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        if (is_from_push) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("is_from_push", true);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
        } else {
            super.onBackPressed();
        }

    }
}
