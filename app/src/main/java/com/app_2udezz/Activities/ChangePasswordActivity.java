package com.app_2udezz.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.app_2udezz.R;
import com.app_2udezz.Util.ConnectivityReceiver;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ChangePasswordActivity extends AppCompatActivity {
    ChangePasswordActivity context;
    private SavePref savePref;
    @BindView(R.id.current_password)
    EditText currentPassword;
    @BindView(R.id.new_password)
    EditText newPassword;
    @BindView(R.id.confirm_new_password)
    EditText confirmNewPassword;
    @BindView(R.id.next_button)
    ImageView nextButton;
    @BindView(R.id.back_button)
    ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_change);
        ButterKnife.bind(this);

        context=ChangePasswordActivity.this;
        savePref=new SavePref(context);
    }

    @OnClick({R.id.next_button, R.id.back_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next_button:
                if (ConnectivityReceiver.isConnected()) {
                    if (currentPassword.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Current Password");
                        nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else if (newPassword.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter New Password");
                        nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else if (confirmNewPassword.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Confirm Password");
                        nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else if (!newPassword.getText().toString().equals(confirmNewPassword.getText().toString())) {
                        util.IOSDialog(context, "Password not match");
                        nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else {
                        CHANGE_PASSWORD_API();
                    }
                }
                else {
                    util.IOSDialog(context,util.internet_Connection_Error);
                }
                break;
            case R.id.back_button:
                finish();
                break;
        }
    }


    private void CHANGE_PASSWORD_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.OLD_PASSWORD, currentPassword.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.NEW_PASSWORD, newPassword.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.CHANGEPASSWORD, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, jsonMainobject.getString("message"));
                            finish();
                            util.hideKeyboard(context);
                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
