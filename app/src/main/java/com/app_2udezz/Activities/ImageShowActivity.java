package com.app_2udezz.Activities;

import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.app_2udezz.R;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageShowActivity extends AppCompatActivity {

    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.back_button)
    ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_image_show);
        ButterKnife.bind(this);

        // image.setImageBitmap(getBitmapFromURL(getIntent().getStringExtra("image")));

        Glide.with(this).load(getIntent().getStringExtra("image")).into(image);
    }


    @OnClick(R.id.back_button)
    public void onClick() {
        finish();
    }
}
