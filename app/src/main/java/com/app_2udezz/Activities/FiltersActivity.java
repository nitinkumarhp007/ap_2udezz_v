package com.app_2udezz.Activities;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Adapters.FiltersAdapter;
import com.app_2udezz.DataModels.FilterModel;
import com.app_2udezz.R;
import com.bumptech.glide.Glide;

import net.alhazmy13.imagefilter.ImageFilter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FiltersActivity extends AppCompatActivity {
    FiltersActivity context;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;

    ArrayList<FilterModel> list;
    @BindView(R.id.image1)
    ImageView image1;
    @BindView(R.id.image2)
    ImageView image2;
    @BindView(R.id.image3)
    ImageView image3;
    @BindView(R.id.image4)
    ImageView image4;
    @BindView(R.id.image5)
    ImageView image5;
    @BindView(R.id.image6)
    ImageView image6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_filters);
        ButterKnife.bind(this);
        context = FiltersActivity.this;
        list = new ArrayList<>();

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading Filters...");
        progressDialog.show();

        String path = getIntent().getStringExtra("filepath");
        Glide.with(context).load(path).into(image);


       /* FilterModel filterModel = new FilterModel();
        try {
            filterModel.setBitmap(ImageFilter.applyFilter(MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                    Uri.fromFile(new File(path))), ImageFilter.Filter.GRAY));
        } catch (IOException e) {
            e.printStackTrace();
        }

        FilterModel filterModel1 = new FilterModel();
        try {
            filterModel1.setBitmap(ImageFilter.applyFilter(MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                    Uri.fromFile(new File(path))), ImageFilter.Filter.RELIEF));
        } catch (IOException e) {
            e.printStackTrace();
        }
        FilterModel filterModel2 = new FilterModel();
        try {
            filterModel2.setBitmap(ImageFilter.applyFilter(MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                    Uri.fromFile(new File(path))), ImageFilter.Filter.AVERAGE_BLUR));
        } catch (IOException e) {
            e.printStackTrace();
        }
        FilterModel filterModel3 = new FilterModel();
        try {
            filterModel3.setBitmap(ImageFilter.applyFilter(MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                    Uri.fromFile(new File(path))), ImageFilter.Filter.OIL));
        } catch (IOException e) {
            e.printStackTrace();
        }
        FilterModel filterModel4 = new FilterModel();
        try {
            filterModel4.setBitmap(ImageFilter.applyFilter(MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                    Uri.fromFile(new File(path))), ImageFilter.Filter.NEON));
        } catch (IOException e) {
            e.printStackTrace();
        }
        FilterModel filterModel5 = new FilterModel();
        try {
            filterModel5.setBitmap(ImageFilter.applyFilter(MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                    Uri.fromFile(new File(path))), ImageFilter.Filter.PIXELATE));
        } catch (IOException e) {
            e.printStackTrace();
        }

        list.add(filterModel);
        list.add(filterModel1);
        list.add(filterModel2);
        list.add(filterModel3);
        list.add(filterModel4);
        list.add(filterModel5);
*/
        Bitmap bitmap1 = null;
        try {
            bitmap1 = ImageFilter.applyFilter(MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                    Uri.fromFile(new File(path))), ImageFilter.Filter.PIXELATE);
        } catch (IOException e) {
            e.printStackTrace();
        }

        image1.setImageBitmap(bitmap1);

        Bitmap bitmap2 = null;
        try {
            bitmap2 = ImageFilter.applyFilter(MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                    Uri.fromFile(new File(path))), ImageFilter.Filter.HDR);
        } catch (IOException e) {
            e.printStackTrace();
        }

        image2.setImageBitmap(bitmap2);
        Bitmap bitmap3 = null;
        try {
            bitmap3 = ImageFilter.applyFilter(MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                    Uri.fromFile(new File(path))), ImageFilter.Filter.SOFT_GLOW);
        } catch (IOException e) {
            e.printStackTrace();
        }

        image3.setImageBitmap(bitmap3);
        Bitmap bitmap4 = null;
        try {
            bitmap4 = ImageFilter.applyFilter(MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                    Uri.fromFile(new File(path))), ImageFilter.Filter.MOTION_BLUR);
        } catch (IOException e) {
            e.printStackTrace();
        }

        image4.setImageBitmap(bitmap4);
        Bitmap bitmap5 = null;
        try {
            bitmap5 = ImageFilter.applyFilter(MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                    Uri.fromFile(new File(path))), ImageFilter.Filter.GRAY);
        } catch (IOException e) {
            e.printStackTrace();
        }

        image5.setImageBitmap(bitmap5);
        Bitmap bitmap6 = null;
        try {
            bitmap6 = ImageFilter.applyFilter(MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                    Uri.fromFile(new File(path))), ImageFilter.Filter.OLD);
        } catch (IOException e) {
            e.printStackTrace();
        }

        image6.setImageBitmap(bitmap6);

        progressDialog.dismiss();

        /*FiltersAdapter adapter = new FiltersAdapter(context, list, bitmap);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        myRecyclerView.setAdapter(adapter);*/
    }
}
