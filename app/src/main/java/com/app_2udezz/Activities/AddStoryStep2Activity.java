package com.app_2udezz.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.app_2udezz.MainActivity;
import com.app_2udezz.R;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddStoryStep2Activity extends AppCompatActivity {
    AddStoryStep2Activity context;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.video_play)
    ImageView videoPlay;
    @BindView(R.id.post_button)
    Button postButton;
    String thumb1 = "", latitude = "", longitude = "", selectedimage = "";
    boolean is_image = false;

    private SavePref savePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_story_step2);

        ButterKnife.bind(this);
        context = AddStoryStep2Activity.this;
        savePref = new SavePref(context);


        is_image = getIntent().getBooleanExtra("is_image", false);
        selectedimage = "";
        selectedimage = getIntent().getStringExtra("filepath");
        if (is_image) {
            Glide.with(context).load(selectedimage).into(image);
        } else {
           Bitmap video_bitmap = ThumbnailUtils.createVideoThumbnail(selectedimage, MediaStore.Video.Thumbnails.MINI_KIND);
           /* Bitmap video_bitmap = null;
            try {
                video_bitmap = retriveVideoFrameFromVideo(selectedimage);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }*/
            thumb1 = getInsertedPath(video_bitmap, "");
            image.setImageBitmap(video_bitmap);
            // Glide.with(context).load(thumb1).into(image);
            videoPlay.setImageDrawable(getResources().getDrawable(R.drawable.video_play));
        }


    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath) throws Throwable
    {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try
        {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    private void ADD_STORY_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);


        if (!is_image) {
            if (!selectedimage.isEmpty()) {
                MediaType MEDIA_TYPE = null;
                MEDIA_TYPE = selectedimage.endsWith("mp4") ? MediaType.parse("/mp4") : MediaType.parse("/mp4");
                File file = new File(selectedimage);
                formBuilder.addFormDataPart(Parameters.POST, file.getName(), RequestBody.create(MEDIA_TYPE, file));
            }

            if (!thumb1.isEmpty()) {
                final MediaType MEDIA_TYPE = thumb1.endsWith("png") ?
                        MediaType.parse("image/png") : MediaType.parse("image/jpeg");

                File file = new File(thumb1);
                formBuilder.addFormDataPart(Parameters.THUMBIMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
            }
            formBuilder.addFormDataPart(Parameters.POST_TYPE, "1");
        } else {
            if (!selectedimage.isEmpty()) {
                final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                        MediaType.parse("image/png") : MediaType.parse("image/jpeg");

                File file = new File(selectedimage);
                formBuilder.addFormDataPart(Parameters.POST, file.getName(), RequestBody.create(MEDIA_TYPE, file));
            }
            formBuilder.addFormDataPart(Parameters.POST_TYPE, "0");
        }
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ADD_STORY, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage("Story Added successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(context, MainActivity.class);
                                    intent.putExtra("from_add_story", true);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                }
                            }).show();
                        } else {
                            if (jsonObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public String getInsertedPath(Bitmap bitmap, String path) {
        String strMyImagePath = null;
        File mFolder = new File(Environment.getExternalStoragePublicDirectory("Ceedpage"), "");

        if (!mFolder.exists()) {
            mFolder.mkdir();
        }

        String s = String.valueOf(System.currentTimeMillis() / 1000) + ".jpg";
        File f = new File(mFolder.getAbsolutePath(), s);
        Log.e("creating", "name " + f.getName());
        strMyImagePath = f.getAbsolutePath();
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (strMyImagePath == null) {
            return path;
        }
        return strMyImagePath;
    }

    @OnClick(R.id.post_button)
    public void onClick() {
        ADD_STORY_API();
    }
}