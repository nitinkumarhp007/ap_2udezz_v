package com.app_2udezz.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Adapters.TrendingAdapter;
import com.app_2udezz.DataModels.PostModel;
import com.app_2udezz.R;
import com.app_2udezz.Util.ConnectivityReceiver;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class TrendingActivity extends AppCompatActivity {
    TrendingActivity context;
    @BindView(R.id.search_bar)
    TextView searchBar;
    @BindView(R.id.search_bar_l)
    LinearLayout searchBarL;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_text)
    TextView errorText;
    private SavePref savePref;

    private ArrayList<PostModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trending);
        ButterKnife.bind(this);

      //  searchBar.setEnabled(false);

        context = TrendingActivity.this;
        savePref = new SavePref(context);


        setToolbar();

        searchBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, SearchUsersActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });

        if (ConnectivityReceiver.isConnected())
            RANDOM_POSTS();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void RANDOM_POSTS() {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.RANDOM_POSTS+"?limit=100000", formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0) {
                    list.clear();
                }
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("data");

                            // total_post = Integer.parseInt(jsonmainObject.getJSONObject("data").getJSONObject("pagination").getString("totalRecord"));

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                PostModel postModel = new PostModel();
                                postModel.setCreated(object.getString("created"));
                                postModel.setDescription(object.getString("description"));
                                postModel.setId(object.getString("id"));
                                postModel.setIs_like(object.getString("is_like"));
                                postModel.setLocations(object.getString("locations"));
                                postModel.setMedia(object.getString("media"));
                                postModel.setPost_type(object.getString("post_type"));
                                postModel.setTitle(object.getString("title"));
                                postModel.setTotal_comments(object.getString("total_comments"));
                                postModel.setTotal_likes(object.getString("total_likes"));
                                postModel.setUser_id(object.getString("user_id"));
                                postModel.setVerfiy_badge(object.optString("verfiy_badge"));

                                if (object.getString("user_id").equals(savePref.getID())) {
                                    postModel.setIs_show_delete(true);
                                } else {
                                    postModel.setIs_show_delete(false);
                                }


                                postModel.setMedia_type(object.optString("media_type"));

                                postModel.setImage(object.getString("profile"));
                                postModel.setName(object.getString("first_name"));
                                list.add(postModel);
                            }

                            if (list.size() > 0) {

                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                                myRecyclerView.setLayoutManager(new GridLayoutManager(context, 3));
                                myRecyclerView.setAdapter(new TrendingAdapter(context, list));

                            } else {
                                errorText.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Search");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

}