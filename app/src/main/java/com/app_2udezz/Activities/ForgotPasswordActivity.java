package com.app_2udezz.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.app_2udezz.R;
import com.app_2udezz.Util.ConnectivityReceiver;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ForgotPasswordActivity extends AppCompatActivity {
    ForgotPasswordActivity context;
    @BindView(R.id.email_address)
    EditText emailAddress;
    @BindView(R.id.next_button)
    ImageView nextButton;
    @BindView(R.id.back_button)
    ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);

        context = ForgotPasswordActivity.this;
    }

    @OnClick({R.id.next_button, R.id.back_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next_button:
                if (ConnectivityReceiver.isConnected()) {
                    if (emailAddress.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Email Address");
                        nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                        util.IOSDialog(context, "Please Enter a Vaild Email Address");
                        nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else {
                        FORGOTPASSWORD_API();
                    }
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
            case R.id.back_button:
                finish();
                break;
        }
    }

    private void FORGOTPASSWORD_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.FORGOT_PASSWORD, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, "Please check your Email inbox");
                            finish();
                        } else {
                            util.IOSDialog(context, jsonObject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
