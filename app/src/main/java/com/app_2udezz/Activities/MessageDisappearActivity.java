package com.app_2udezz.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.app_2udezz.R;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MessageDisappearActivity extends AppCompatActivity {
    MessageDisappearActivity context;
    private SavePref savePref;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.permanent)
    RadioButton permanent;
    @BindView(R.id._5_seconds)
    RadioButton _5Seconds;
    @BindView(R.id._10_seconds)
    RadioButton _10Seconds;
    @BindView(R.id._30_seconds)
    RadioButton _30Seconds;
    @BindView(R.id._1_minutes)
    RadioButton _1Minutes;
    @BindView(R.id._5_minutes)
    RadioButton _5Minutes;
    @BindView(R.id._30_minutes)
    RadioButton _30Minutes;
    @BindView(R.id._1_hour)
    RadioButton _1Hour;
    @BindView(R.id._24_hours)
    RadioButton _24Hours;
    @BindView(R.id.submit)
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_message_disappear);
        ButterKnife.bind(this);

        context = MessageDisappearActivity.this;
        savePref = new SavePref(context);

        if (savePref.getStringLatest("delete_time").equals("0")|| savePref.getStringLatest("delete_time").equals("")) {
            permanent.setChecked(true);
        } else if (savePref.getStringLatest("delete_time").equals("5")) {
            _5Seconds.setChecked(true);
        } else if (savePref.getStringLatest("delete_time").equals("10")) {
            _10Seconds.setChecked(true);
        } else if (savePref.getStringLatest("delete_time").equals("30")) {
            _30Seconds.setChecked(true);
        } else if (savePref.getStringLatest("delete_time").equals("60")) {
            _1Minutes.setChecked(true);
        } else if (savePref.getStringLatest("delete_time").equals("300")) {
            _5Minutes.setChecked(true);
        } else if (savePref.getStringLatest("delete_time").equals("1800")) {
            _30Minutes.setChecked(true);
        } else if (savePref.getStringLatest("delete_time").equals("3600")) {
            _1Hour.setChecked(true);
        } else if (savePref.getStringLatest("delete_time").equals("86400")) {
            _24Hours.setChecked(true);
        }

        setToolbar();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Message Disappear");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.submit)
    public void onClick() {
        EDIT_PROFILE_API();
    }

    private void EDIT_PROFILE_API() {
        String delete_time = "";
        if (permanent.isChecked()) {
            delete_time = "0";
        } else if (_5Seconds.isChecked()) {
            delete_time = "5";
        } else if (_10Seconds.isChecked()) {
            delete_time = "10";
        } else if (_30Seconds.isChecked()) {
            delete_time = "30";
        } else if (_1Minutes.isChecked()) {
            delete_time = "60";
        } else if (_5Minutes.isChecked()) {
            delete_time = "300";
        } else if (_30Minutes.isChecked()) {
            delete_time = "1800";
        } else if (_1Hour.isChecked()) {
            delete_time = "3600";
        } else if (_24Hours.isChecked()) {
            delete_time = "86400";
        }

        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.DELETE_TIME, delete_time);//is_private
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.EDIT_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("data");
                            savePref.setStringLatest("delete_time", body.getString("delete_time"));
                            finish();
                            util.showToast(context, "Setting Updated Successfully!");
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
