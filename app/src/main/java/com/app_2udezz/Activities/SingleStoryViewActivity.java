package com.app_2udezz.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app_2udezz.DataModels.PostModel;
import com.app_2udezz.DataModels.StoryModel;
import com.app_2udezz.R;
import com.app_2udezz.Util.ConnectivityReceiver;
import com.app_2udezz.Util.KeyboardUtils;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;
import com.appunite.appunitevideoplayer.PlayerActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.shts.android.storiesprogressview.StoriesProgressView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SingleStoryViewActivity extends AppCompatActivity {
    SingleStoryViewActivity context;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.stories)
    StoriesProgressView stories;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.video_play)
    ImageView videoPlay;
    @BindView(R.id.v1)
    View v1;
    @BindView(R.id.v2)
    View v2;
    @BindView(R.id.type_message)
    EditText typeMessage;
    @BindView(R.id.send_button)
    ImageView sendButton;
    private SavePref savePref;

    private ArrayList<PostModel> post_list;
    private ArrayList<StoryModel> list;
    public int position_item = 0;
    public int count = 0;
    boolean is_friend = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_story_view);
        ButterKnife.bind(this);

        context = SingleStoryViewActivity.this;
        savePref = new SavePref(context);

        post_list = getIntent().getParcelableArrayListExtra("list");

        position_item = getIntent().getIntExtra("position", 0);
        is_friend = getIntent().getBooleanExtra("is_friend", false);


        if (post_list.get(position_item).getList().get(count).getPost_type().equals("1")) {
            setImage_video(count);
        } else {
            setImage(count);
        }

        stories.setStoriesCount(post_list.get(position_item).getList().size()); // <- set stories
        stories.setStoryDuration(5000); // <- set a story duration
        stories.startStories();


        stories.setStoriesListener(new StoriesProgressView.StoriesListener() {
            @Override
            public void onNext() {
                if (count != (post_list.get(position_item).getList().size() - 1)) {
                    count = count + 1;
                    if (post_list.get(position_item).getList().get(count).getPost_type().equals("1")) {
                        setImage_video(count);
                        Log.e("calllll", "onNext setImage_video" + String.valueOf(count));
                    } else {
                        setImage(count);
                        Log.e("calllll", "onNext setImage" + String.valueOf(count));
                    }


                }

            }

            @Override
            public void onPrev() {
                if (count > 0) {
                    count = count - 1;
                    if (post_list.get(position_item).getList().get(count).getPost_type().equals("1")) {
                        setImage_video(count);
                        Log.e("calllll", "onPrev setImage_video " + String.valueOf(count));
                    } else {
                        setImage(count);
                        Log.e("calllll", "onPrev setImage" + String.valueOf(count));
                    }
                }

            }

            @Override
            public void onComplete() {
                Log.e("calllll", "onComplete " + String.valueOf(count));

                if (count == (post_list.get(position_item).getList().size() - 1)) {
                    finish();
                   /* position_item = position_item + 1;
                    count = 0;
                    stories.setStoriesCount(post_list.get(position_item).getList().size()); // <- set stories
                    setImage(count);
                    stories.startStories(); // <- start progress*/
                }

            }
        }); // <- set listener

        // <- start progress

        KeyboardUtils.addKeyboardToggleListener(context, new KeyboardUtils.SoftKeyboardToggleListener() {
            @Override
            public void onToggleSoftKeyboard(boolean isVisible) {
                Log.d("keyboard", "keyboard visible: " + isVisible);

                if (isVisible) {
                    stories.pause();
                } else {
                    stories.resume();
                }

            }
        });

        videoPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!post_list.get(position_item).getList().get(count).getPost().isEmpty()) {
                    util.story_pause = true;
                    util.pos = count;
                    stories.pause();
                    context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
                            post_list.get(position_item).getList().get(count).getPost(),
                            " ", 0));
                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (util.story_pause) {
            util.story_pause = false;
            if (stories != null)
                stories.startStories(util.pos);

        }
    }

    private void setImage(int position) {
        image.setVisibility(View.VISIBLE);
        videoPlay.setVisibility(View.INVISIBLE);
        if (post_list.get(context.position_item).getList().size() > position) {
//            Log.e("image__", post_list.get(context.position_item).getList().get(position).getPost());
            Glide.with(context)
                    .load(post_list.get(context.position_item).getList().get(position).getPost())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(image);
        }
        //Glide.with(context).load(list.get(position).getPost()).into(image);
    }

    private void setImage_video(int position) {
        image.setVisibility(View.VISIBLE);
        videoPlay.setVisibility(View.VISIBLE);
        if (post_list.get(context.position_item).getList().size() > position) {
            Log.e("getThumbImage", post_list.get(context.position_item).getList().get(position).getThumbImage());
//            Log.e("image__", post_list.get(context.position_item).getList().get(position).getPost());
            Glide.with(context)
                    .load(post_list.get(context.position_item).getList().get(position).getThumbImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(image);
        }
        //Glide.with(context).load(list.get(position).getPost()).into(image);
    }

   /* private void setVideoPlay(int position) {
        stories.pause();
        if (post_list.get(context.position_item).getList().size() > position) {
            // videoView.setVideoPath(post_list.get(context.position_item).getList().get(position).getPost());
            videoView.setVideoPath("http://3.12.247.224/uploads/1603872490715.mp4");
            videoView.setMediaController(new MediaController(this));


            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    videoView.seekTo(1);
                    if (count == 0)
                        stories.startStories();
                    else
                        stories.resume();
                }
            });

            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {

                }
            });

        }
        //Glide.with(context).load(list.get(position).getPost()).into(image);
    }*/


    @OnClick({R.id.video_play, R.id.v1, R.id.v2, R.id.send_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.video_play:
                break;
            case R.id.v1:
                stories.reverse();
                break;
            case R.id.v2:
                stories.skip();
                break;
            case R.id.send_button:
                if (ConnectivityReceiver.isConnected()) {
                    if (!typeMessage.getText().toString().isEmpty()) {
                        String comment = typeMessage.getText().toString().trim();
                        typeMessage.setText("");
                        context.POST_COMMENT(comment,
                                post_list.get(context.position_item).getList().get(count).getId(), post_list.get(context.position_item).getList().get(count).getUser_id());
                    }

                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        // Very important !
        stories.destroy();
        super.onDestroy();
    }

    public void POST_COMMENT(String comment, String story_id, String user_id) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.COMMENT, comment);
        formBuilder.addFormDataPart(Parameters.STORY_ID, story_id);
        formBuilder.addFormDataPart(Parameters.USER_ID, user_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.STORY_REPLY, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                util.hideKeyboard(context);
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject object = jsonmainObject.getJSONObject("data");
                            util.showToast(context, "Messsage sent");
                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

}