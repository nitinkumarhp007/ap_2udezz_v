package com.app_2udezz.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app_2udezz.Fragments.AddFragment;
import com.app_2udezz.R;
import com.app_2udezz.Util.FileUtils;
import com.app_2udezz.Util.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AddStoryActivity extends AppCompatActivity implements SurfaceHolder.Callback {

    @BindView(R.id.camera_icon)
    ImageView cameraIcon;
    @BindView(R.id.relative_top)
    RelativeLayout relativeTop;
    @BindView(R.id.surfaceView)
    SurfaceView surfaceView;
    @BindView(R.id.timer_starting)
    TextView timerStarting;
    @BindView(R.id.captureImage)
    ImageView captureImage;
    AddStoryActivity context;
    Unbinder unbinder;
    @BindView(R.id.camera_layout)
    RelativeLayout cameraLayout;
    @BindView(R.id.video_layout)
    RelativeLayout videoLayout;
    @BindView(R.id.gallery)
    ImageView gallery;
    @BindView(R.id.switch_)
    ImageView switch_;
    private Camera camera;
    private int cameraId;
    private int rotation;
    private int rotation2;
    private File imageFile;
    MediaRecorder mediaRecorder;
    boolean is_back = false;
    long lastDown;
    long lastDuration;
    long millisUntilFinished = 0;
    Timer timer;
    String video_url = "", thumb = "";
    Bitmap video_bitmap;
    int width, height;
    SurfaceHolder arg0;

    boolean is_video = false;

    CountDownTimer countDownTimer = null;

    private SurfaceHolder surfaceHolder;
    private int MEDIA_TYPE_GALLERY = 1947;
    boolean is_image = true;
    boolean is_started = false;
    File file = null;

    private int VIDEO_CAPTURE = 200;

    boolean is_front = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_story);
        ButterKnife.bind(this);
        context = AddStoryActivity.this;

        cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        switch_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (is_front) {
                    is_front = false;
                    if (!openCamera(Camera.CameraInfo.CAMERA_FACING_BACK, true)) {
                        alertCameraDialog();
                    }
                } else {
                    is_front = true;
                    if (!openCamera(Camera.CameraInfo.CAMERA_FACING_FRONT, true)) {
                        alertCameraDialog();
                    }
                }

            }
        });
        setToolbar();

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Add Story");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseCamera();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!openCamera(Camera.CameraInfo.CAMERA_FACING_BACK, true)) {
            alertCameraDialog();
        }
    }

    private void setUpCamera(Camera c, boolean is_camera) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degree = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degree = 0;
                break;
            case Surface.ROTATION_90:
                degree = 90;
                break;
            case Surface.ROTATION_180:
                degree = 180;
                break;
            case Surface.ROTATION_270:
                degree = 270;
                break;

            default:
                break;
        }

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            // frontFacing
            rotation = (info.orientation + degree) % 330;
            rotation = (360 - rotation) % 360;
        } else {
            // Back-facing
            rotation = (info.orientation - degree + 360) % 360;
        }
        rotation2 = (info.orientation - degree + 360) % 360;
        c.setDisplayOrientation(rotation);
        Camera.Parameters params = c.getParameters();


        List<String> focusModes = params.getSupportedFlashModes();
        if (focusModes != null) {
            if (is_camera) {
                if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                }
            } else {
                if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                }
            }

        }

        //params.setRotation(rotation);

        camera.setParameters(params);
    }


    private void releaseCamera() {
        try {
            if (camera != null) {
                camera.setPreviewCallback(null);
                camera.setErrorCallback(null);
                camera.stopPreview();
                camera.release();
                camera = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("error", e.toString());
            camera = null;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder arg0_, int format_, int width_, int height_) {

        arg0 = arg0_;
        height = height_;
        width = width_;

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }


    protected void startRecording() throws IOException {
        start_timer();
        if (camera == null)
            camera = Camera.open();

        File dir = context.getExternalCacheDir();
        //File dir = new File(context.getExternalCacheDir() + "/2udezzVideos");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        Date date = new Date();
        String fileName = "/rec" + date.toString().replace(" ", "_").replace(":", "_") + ".mp4";
        file = new File(dir, fileName);

        mediaRecorder = new MediaRecorder();
        camera.lock();
        camera.unlock();
        // Please maintain sequence of following code.
        // If you change sequence it will not work.
        mediaRecorder.setCamera(camera);


        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);

        mediaRecorder.setVideoEncodingBitRate(3000000);
        mediaRecorder.setVideoSize(1920, 1080);
        mediaRecorder.setPreviewDisplay(surfaceHolder.getSurface());
        mediaRecorder.setOutputFile(dir + fileName);
        mediaRecorder.setOrientationHint(90);
        mediaRecorder.setMaxDuration(10000);

        mediaRecorder.setVideoFrameRate(30);
        mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        mediaRecorder.prepare();

        try {
            mediaRecorder.start();
        } catch (Exception e) {
        }

        mediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
            @Override
            public void onInfo(MediaRecorder mr, int what, int extra) {
                if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                    Log.v("VIDEOCAPTURE", "Maximum Duration Reached");
                    mr.stop();
                }
            }
        });


    }

    private void start_timer() {
        countDownTimer = new CountDownTimer(10000, 1000) {
            public void onTick(long millisUntilFinished) {
                if (timerStarting != null)
                    timerStarting.setText("00:" + String.valueOf(millisUntilFinished / 1000));
            }

            public void onFinish() {
                stopRecording();
            }
        }.start();


    }

    private void refreshGallery(File file) {
        Intent mediaScanIntent = new Intent(
                Intent.ACTION_MEDIA_SCANNER_STARTED);
        mediaScanIntent.setData(Uri.fromFile(file));
        Log.e("file__", file.toString());
        video_url = file.getAbsolutePath();
        sendBroadcast(mediaScanIntent);

        //  video_url = file.getAbsolutePath();

      /*  Bundle bundle = new Bundle();
        bundle.putString("filepath", video_url);
        bundle.putBoolean("is_image", is_image);
        bundle.putString("file_name", imageFile + ".mp4");

        //  Toast.makeText(context, "Video Sucessfully Recorded", Toast.LENGTH_SHORT).show();
        AddFragment fragment = new AddFragment();
        fragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();*/


        Intent intent = new Intent(context, AddStoryStep2Activity.class);
        intent.putExtra("filepath", video_url);
        intent.putExtra("is_image", is_image);
        intent.putExtra("file_name", imageFile + ".mp4");
        startActivity(intent);

        //sendBroadcast(mediaScanIntent);
    }


    private boolean openCamera(int id, boolean is_camera) {
        boolean result = false;
        cameraId = id;
        releaseCamera();
        try {
            camera = Camera.open(cameraId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (camera != null) {
            try {
                setUpCamera(camera, is_camera);
                camera.setErrorCallback(new Camera.ErrorCallback() {

                    @Override
                    public void onError(int error, Camera camera) {

                    }
                });
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();
                result = true;
            } catch (IOException e) {
                e.printStackTrace();
                result = false;
                releaseCamera();
            }
        }
        return result;
    }


    private void takeImage() {

        camera.takePicture(null, null, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                try {
                    // convert byte array into bitmap
                    Bitmap loadedImage = null;
                    Bitmap rotatedBitmap = null;
                    loadedImage = BitmapFactory.decodeByteArray(data, 0,
                            data.length);
                    // rotate Image
                    Matrix rotateMatrix = new Matrix();
                    rotateMatrix.postRotate(rotation2);
                    rotatedBitmap = Bitmap.createBitmap(loadedImage, 0, 0,
                            loadedImage.getWidth(), loadedImage.getHeight(),
                            rotateMatrix, false);
                    String state = Environment.getExternalStorageState();
                    File folder = null;
                    if (state.contains(Environment.MEDIA_MOUNTED)) {
                        folder = new File(context.getExternalCacheDir() + "/2udezz");

                    } else {

                        folder = new File(context.getExternalCacheDir() + "/2udezz");
                    }

                    boolean success = true;

                    if (!folder.exists()) {
                        success = folder.mkdirs();
                    }

                    if (success) {

//
//                    file path image  "imageFile"

                        Date date = new Date();
                        imageFile = new File(folder.getAbsolutePath()
                                + File.separator
                                + new Timestamp(date.getTime()).toString()
                                + "Image.jpg");
                        //Toast.makeText(CameraActivity.this, ""+imageFile, Toast.LENGTH_SHORT).show();
                        String value = imageFile.getAbsolutePath();


                      /*  Intent intent = new Intent(context, FiltersActivity.class);
                        intent.putExtra("filepath", value);
                        intent.putExtra("file_name", imageFile + ".jpg");
                        startActivity(intent);*/


                      /*  Bundle bundle = new Bundle();
                        bundle.putString("filepath", value);
                        bundle.putBoolean("is_image", is_image);
                        bundle.putString("file_name", imageFile + ".jpg");
                        AddFragment fragment = new AddFragment();
                        fragment.setArguments(bundle);
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_container, fragment);
                        transaction.commit();
*/
                        Intent intent = new Intent(context, AddStoryStep2Activity.class);
                        intent.putExtra("filepath", value);
                        intent.putExtra("is_image", is_image);
                        intent.putExtra("file_name", imageFile + ".jpg");
                        startActivity(intent);

                        Log.e("getfile_pathVar", "" + value);
                        //imageFile.createNewFile();
                    } else {

                        Toast.makeText(context, "Image Not saved",
                                Toast.LENGTH_SHORT).show();

                        return;
                    }

                    ByteArrayOutputStream ostream = new ByteArrayOutputStream();

                    // save image into gallery
                    rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);

                    FileOutputStream fout = new FileOutputStream(imageFile);
                    fout.write(ostream.toByteArray());
                    fout.close();
                    ContentValues values = new ContentValues();

                    values.put(MediaStore.Images.Media.DATE_TAKEN,
                            System.currentTimeMillis());
                    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                    values.put(MediaStore.MediaColumns.DATA,
                            imageFile.getAbsolutePath());

                    context.getContentResolver().insert(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MEDIA_TYPE_GALLERY) {
            if (resultCode == RESULT_OK) {

                // File myFile = new File(data.getData().getPath());
                //String selectedimage = myFile.getAbsolutePath();

                String selectedimage = null;

                selectedimage = getRealPathFromURI1(data.getData());

               /* Bundle bundle = new Bundle();
                bundle.putString("filepath", selectedimage);
                bundle.putBoolean("is_image", is_image);
                bundle.putString("file_name", imageFile + ".jpg");
                AddFragment fragment = new AddFragment();
                fragment.setArguments(bundle);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.commit();*/


                Intent intent = new Intent(context, AddStoryStep2Activity.class);
                intent.putExtra("filepath", selectedimage);
                intent.putExtra("is_image", is_image);
                intent.putExtra("file_name", imageFile + ".jpg");
                startActivity(intent);

            } else if (resultCode == RESULT_CANCELED) {
                /*Toast.makeText(getApplicationContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();*/
            }
        } else if (requestCode == VIDEO_CAPTURE) {
            if (resultCode == RESULT_OK) {

                String selectedimage = null;
                if (data != null) {
                    selectedimage = FileUtils.getPath(context, data.getData());

                    File f = new File(selectedimage);
                    long fileSizeInBytes = f.length();
                    long fileSizeInKB = fileSizeInBytes / 1024;
                    long fileSizeInMB = fileSizeInKB / 1024;
                    if (fileSizeInMB > 10) {
                        util.IOSDialog(context, "Sorry file size is large, Use Video under 10 MB");
                    } else {
                      /*  Bundle bundle = new Bundle();
                        bundle.putString("filepath", selectedimage);
                        bundle.putBoolean("is_image", is_image);
                        bundle.putString("file_name", imageFile + ".mp4");
                        AddFragment fragment = new AddFragment();
                        fragment.setArguments(bundle);
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_container, fragment);
                        transaction.commit();*/

                        Intent intent = new Intent(context, AddStoryStep2Activity.class);
                        intent.putExtra("filepath", selectedimage);
                        intent.putExtra("is_image", is_image);
                        intent.putExtra("file_name", imageFile + ".mp4");
                        startActivity(intent);
                    }
                }
            }
        }
    }

    private String getRealPathFromURI1(Uri contentURI) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public String getPath_video(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else return null;
    }


    public String getRealPathFromURI(Uri uri) {
        File file = new File(uri.getPath());//create path from uri
        final String[] split = file.getPath().split(":");//split the path.
        return split[1];
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private void alertCameraDialog() {
        AlertDialog.Builder dialog = createAlert(context, "Camera info", "error to open camera");

        dialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

        dialog.show();
    }

    private AlertDialog.Builder createAlert(Context context, String title, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
        dialog.setIcon(R.mipmap.ic_launcher);

        if (title != null)
            dialog.setTitle(title);
        else
            dialog.setTitle("Information");

        dialog.setMessage(message);
        dialog.setCancelable(false);
        return dialog;
    }

    @OnClick({R.id.camera_layout, R.id.gallery, R.id.video_layout, R.id.captureImage})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.camera_layout:
                cameraLayout.setBackground(getResources().getDrawable(R.drawable.drawable_edittext));
                videoLayout.setBackground(null);
                is_image = true;
                if (!openCamera(Camera.CameraInfo.CAMERA_FACING_BACK, true)) {
                    alertCameraDialog();
                }

                break;
            case R.id.gallery:
                //Options();
                is_image = true;
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, MEDIA_TYPE_GALLERY);
                break;
            case R.id.video_layout:
                videoLayout.setBackground(getResources().getDrawable(R.drawable.drawable_edittext));
                cameraLayout.setBackground(null);
                is_image = false;

                if (!openCamera(Camera.CameraInfo.CAMERA_FACING_BACK, false)) {
                    alertCameraDialog();
                }
                break;
            case R.id.captureImage:
                //captureImage.setEnabled(false);
                if (is_image) {
                    takeImage();
                } else {
                    try {
                        if (!is_started) {
                            is_started = true;
                            captureImage.setImageDrawable(getResources().getDrawable(R.drawable.stop));
                            startRecording();
                        } else {
                            is_started = false;
                            stopRecording();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //takeImage();
                }
                break;
        }
    }


    private void Options() {
        String[] options = {"Image", "Video"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    is_image = true;
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, MEDIA_TYPE_GALLERY);
                } else if (which == 1) {
                    CaptureVideo();
                }
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void CaptureVideo() {
        is_image = false;
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Video"), VIDEO_CAPTURE);
    }


    protected void stopRecording() {
        if (countDownTimer != null)
            countDownTimer.cancel();

        if (mediaRecorder != null) {
            try {
                mediaRecorder.stop();
            } catch (Exception e) {
            }

            mediaRecorder.release();
            camera.release();
            //camera.lock();
            refreshGallery(file);
            // mCamera.lock();
        }
    }

}
