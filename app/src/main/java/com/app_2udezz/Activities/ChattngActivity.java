package com.app_2udezz.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Adapters.AdapterChatting;
import com.app_2udezz.DataModels.ChattingModel;
import com.app_2udezz.DataModels.PostModel;
import com.app_2udezz.DataModels.StoryModel;
import com.app_2udezz.MainActivity;
import com.app_2udezz.R;
import com.app_2udezz.Util.ConnectivityReceiver;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;
import com.app_2udezz.parser.GetAsyncDELETE;
import com.app_2udezz.parser.GetAsyncGet;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ChattngActivity extends AppCompatActivity {
    ChattngActivity context;
    SavePref savePref;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.type_message)
    EditText typeMessage;
    AdapterChatting adapter;
    @BindView(R.id.send_button)
    ImageView sendButton;
    @BindView(R.id.title)
    TextView title;
    Vibrator v;
    @BindView(R.id.attachment)
    ImageView attachment;
    private ArrayList<ChattingModel> chat_list;
    private ArrayList<PostModel> story_list;

    boolean is_from_push = false;
    String friend_id = "";
    private int VIDEO_CAPTURE = 200;
    private String selectedimage = "", thumb1 = "";

    private ProgressDialog progressDialog;

    Uri fileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chattng);
        ButterKnife.bind(this);

        context = ChattngActivity.this;
        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        savePref = new SavePref(context);
        progressDialog = util.initializeProgress(context);

        friend_id = getIntent().getStringExtra("friend_id");
        title.setText(getIntent().getStringExtra("friend_name"));
        is_from_push = getIntent().getBooleanExtra("is_from_push", false);

        LocalBroadcastManager.getInstance(context).registerReceiver(mMessageReceiver,
                new IntentFilter(util.NOTIFICATION_MESSAGE));

        if (ConnectivityReceiver.isConnected())
            GET_MESSAGE();
        else
            util.IOSDialog(context, util.internet_Connection_Error);


    }


    private void GET_MESSAGE() {
        progressDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.FRIEND_ID, friend_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.GET_MESSAGE + "?friend_id=" + friend_id, formBody) {
            @Override
            public void getValueParse(String result) {
                progressDialog.dismiss();
                chat_list = new ArrayList<>();
                if (chat_list.size() > 0)
                    chat_list.clear();

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray jsonbody1 = jsonmainObject.getJSONArray("data");


                            for (int i = 0; i < jsonbody1.length(); i++) {
                                JSONObject jsonbody = jsonbody1.getJSONObject(i);

                                ChattingModel chattingModel = new ChattingModel();
                                chattingModel.setChat_type(jsonbody.getString("message_type"));
                                chattingModel.setmessage_id(jsonbody.getString("id"));
                                chattingModel.setMessage(jsonbody.getString("message"));
                                chattingModel.setVideo_thumbnail(jsonbody.optString("thumb_image"));
                                chattingModel.setTimestamp(jsonbody.getString("created"));

                                if (jsonbody.getString("message_type").equals("4")) {
                                    chattingModel.setStory_media(jsonbody.getJSONObject("storyInfo").getString("post"));
                                    chattingModel.setStory_thumbImage(jsonbody.getJSONObject("storyInfo").getString("thumbImage"));
                                    chattingModel.setStory_post_type(jsonbody.getJSONObject("storyInfo").getString("post_type"));


                                    /*JSONObject object = jsonbody.getJSONObject("storyInfo");
                                    PostModel postModel = new PostModel();
                                    postModel.setImage("");
                                    postModel.setName("");
                                    postModel.setUser_id("");

                                    ArrayList<StoryModel> list = new ArrayList<>();
                                    StoryModel storyModel = new StoryModel();
                                    storyModel.setId(object.getString("id"));
                                    storyModel.setPost(object.getString("post"));
                                    storyModel.setPost_type(object.getString("post_type"));
                                    storyModel.setUser_id(object.getString("user_id"));
                                    storyModel.setImage("");
                                    storyModel.setName("");
                                    storyModel.setThumbImage(object.optString("thumbImage"));
                                    storyModel.setTotalViews(object.optString("totalViews"));
                                    list.add(storyModel);

                                    postModel.setList(list);
                                    story_list.add(postModel);*/

                                }


                                if (savePref.getID().equalsIgnoreCase(jsonbody.getString("sender_id"))) {
                                    chattingModel.setIs_own_message(true);
                                    chattingModel.setOpponent_name(savePref.getFirstName() + " " + savePref.getLastName());
                                } else {
                                    chattingModel.setIs_own_message(false);
                                    chattingModel.setOpponent_name(jsonbody.getJSONObject("friendInfo").getString("first_name"));
                                }
                                chat_list.add(chattingModel);
                            }
                            if (chat_list.size() > 0) {
                                adapter = new AdapterChatting(context, chat_list);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.scrollToPosition(adapter.getItemCount() - 1);
                                myRecyclerView.setAdapter(adapter);
                            }
                        } else {

                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void Options() {
        String[] options = {"Image", "Video"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    CropImage.activity(fileUri)
                            .setAspectRatio(2, 2)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(context);
                } else if (which == 1) {
                    CaptureVideo();
                }
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                selectedimage = getAbsolutePath(this, resultUri);

                SEND_MESSAGE("1");
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        } else if (requestCode == VIDEO_CAPTURE) {
            if (resultCode == RESULT_OK) {
                selectedimage = getAbsolutePath(this, data.getData());
                Bitmap video_bitmap = ThumbnailUtils.createVideoThumbnail(selectedimage, MediaStore.Video.Thumbnails.MINI_KIND);
                thumb1 = getInsertedPath(video_bitmap, "");
                SEND_MESSAGE("2");
            }
        }
    }

    private void CaptureVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra("android.intent.extra.durationLimit", 7);//limit cmaera to capture limited video
        startActivityForResult(intent, VIDEO_CAPTURE);
    }


    private void SEND_MESSAGE(String type) {
        progressDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);


        if (type.equals("2")) {
            if (!selectedimage.isEmpty()) {
                MediaType MEDIA_TYPE = null;
                MEDIA_TYPE = selectedimage.endsWith("mp4") ? MediaType.parse("/mp4") : MediaType.parse("/mp4");
                File file = new File(selectedimage);
                formBuilder.addFormDataPart(Parameters.MESSAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
            }

            if (!thumb1.isEmpty()) {
                final MediaType MEDIA_TYPE = thumb1.endsWith("png") ?
                        MediaType.parse("image/png") : MediaType.parse("image/jpeg");

                File file = new File(thumb1);
                formBuilder.addFormDataPart(Parameters.THUMB_IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
            }
        } else if (type.equals("1")) {
            if (!selectedimage.isEmpty()) {
                MediaType MEDIA_TYPE = null;
                MEDIA_TYPE = selectedimage.endsWith("png") ? MediaType.parse("image/png") : MediaType.parse("image/jpeg");
                File file = new File(selectedimage);
                formBuilder.addFormDataPart(Parameters.MESSAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
            }
        } else {
            formBuilder.addFormDataPart(Parameters.MESSAGE, typeMessage.getText().toString().trim());
        }
        formBuilder.addFormDataPart(Parameters.FRIEND_ID, friend_id);
        formBuilder.addFormDataPart(Parameters.MESSAGE_TYPE, type);//0->text 1-> image 2-> video
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.SEND_MESSAGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                progressDialog.dismiss();
                typeMessage.setText("");
                selectedimage = "";
                thumb1 = "";
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject jsonbody = jsonmainObject.getJSONObject("data");

                            ChattingModel chattingModel = new ChattingModel();
                            chattingModel.setChat_type(jsonbody.getString("message_type"));
                            chattingModel.setmessage_id(jsonbody.getString("id"));
                            chattingModel.setMessage(jsonbody.getString("message"));
                            chattingModel.setTimestamp(jsonbody.getString("created"));
                            chattingModel.setVideo_thumbnail(jsonbody.optString("thumb_image"));
                            chattingModel.setOpponent_name(savePref.getFirstName() + " " + savePref.getLastName());
                            chattingModel.setIs_own_message(true);

                            chat_list.add(chattingModel);
                            if (chat_list.size() > 1) {
                                adapter.notifyDataSetChanged();
                                myRecyclerView.scrollToPosition(adapter.getItemCount() - 1);
                            } else {
                                adapter = new AdapterChatting(context, chat_list);
                                myRecyclerView.scrollToPosition(adapter.getItemCount() - 1);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);
                            }
                        } else {

                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void DeleteAlert(int position) {
        new IOSDialog.Builder(context)
                .setCancelable(false)
                .setMessage("Are you sure to Delete Message?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DELETE_PRODUCT_API(position);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void DELETE_PRODUCT_API(int position) {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHAT_ID, chat_list.get(position).getmessage_id());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncDELETE mAsync = new GetAsyncDELETE(context, AllAPIS.DELETE_MESSAGE, formBody, mDialog, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(Response response) {
                mDialog.dismiss();
                String result = response.body().toString();
                if (response.isSuccessful()) {
                    new IOSDialog.Builder(context)
                            .setCancelable(false)
                            .setMessage("Message Deleted Successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            chat_list.remove(position);
                            adapter.notifyDataSetChanged();
                        }
                    }).show();
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    protected void onResume() {
        super.onResume();
        savePref.isChatScreen(true);
        Log.e("Check", "true");
        savePref.setCHAT_ID(friend_id);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        savePref.isChatScreen(false);
        Log.e("Check", "false");
        savePref.setCHAT_ID("");
        LocalBroadcastManager.getInstance(context).unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                r.play();
                // Vibrate for 400 milliseconds
                v.vibrate(400);
                ChattingModel chattingModel = new ChattingModel();
                chattingModel.setmessage_id(intent.getStringExtra("message_id"));
                chattingModel.setMessage(intent.getStringExtra("message"));
                chattingModel.setTimestamp(intent.getStringExtra("created"));
                chattingModel.setOpponent_name(intent.getStringExtra("username"));
                chattingModel.setChat_type(intent.getStringExtra("message_type"));
                chattingModel.setVideo_thumbnail(intent.getStringExtra("thumb_image"));
                chattingModel.setIs_own_message(false);
                chat_list.add(chattingModel);
                adapter.notifyDataSetChanged();
                myRecyclerView.scrollToPosition(adapter.getItemCount() - 1);
            } catch (NullPointerException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    @OnClick({R.id.attachment, R.id.back_button, R.id.send_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.attachment:
                Options();
                break;
            case R.id.back_button:
                if (!is_from_push) {
                    finish();
                } else {
                    Intent intent = new Intent(ChattngActivity.this, MainActivity.class);
                    intent.putExtra("is_from_push", true);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }

                break;
            case R.id.send_button:
                if (ConnectivityReceiver.isConnected()) {
                    if (!typeMessage.getText().toString().isEmpty()) {
                        SEND_MESSAGE("0");
                    }
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (!is_from_push) {
            super.onBackPressed();
        } else {
            Intent intent = new Intent(ChattngActivity.this, MainActivity.class);
            intent.putExtra("is_from_push", true);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public String getInsertedPath(Bitmap bitmap, String path) {
        String strMyImagePath = null;
        File mFolder = new File(context.getExternalCacheDir() + "/2udezz");

        if (!mFolder.exists()) {
            mFolder.mkdir();
        }

        String s = String.valueOf(System.currentTimeMillis() / 1000) + ".jpg";
        File f = new File(mFolder.getAbsolutePath(), s);
        Log.e("creating", "name " + f.getName());
        strMyImagePath = f.getAbsolutePath();
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (strMyImagePath == null) {
            return path;
        }
        return strMyImagePath;
    }

}
