package com.app_2udezz.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Adapters.CommentAdapter;
import com.app_2udezz.Adapters.MentionUserAdapter;
import com.app_2udezz.Adapters.SearchUsersAdapter;
import com.app_2udezz.Adapters.UserAdapter;
import com.app_2udezz.DataModels.ChattingModel;
import com.app_2udezz.DataModels.CommentModel;
import com.app_2udezz.DataModels.UserModel;
import com.app_2udezz.Fragments.ChatFragment;
import com.app_2udezz.R;
import com.app_2udezz.Util.ConnectivityReceiver;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;
import com.app_2udezz.parser.GetAsyncDELETE;
import com.app_2udezz.parser.GetAsyncGet;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CommentsActivity extends AppCompatActivity {
    CommentsActivity context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.type_message)
    EditText typeMessage;
    @BindView(R.id.send_button)
    ImageView sendButton;
    @BindView(R.id.attachment)
    ImageView attachment;
    private SavePref savePref;
    private ArrayList<CommentModel> list;
    private ArrayList<UserModel> user_list;
    String post_id = "";

    private int VIDEO_CAPTURE = 200;
    private String selectedimage = "", thumb1 = "";
    CommentAdapter Adapter = null;

    String text_inputted = "";
    ProgressDialog mDialog;
    MentionUserAdapter mentionUserAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_comments);
        ButterKnife.bind(this);
        context = CommentsActivity.this;
        savePref = new SavePref(context);

        post_id = getIntent().getStringExtra("post_id");
        mDialog = util.initializeProgress(context);
        setToolbar();

        if (ConnectivityReceiver.isConnected()) {
            GET_COMMENTAPI();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

        typeMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
              /*  if (s.toString().length() > 1) {
                    if (s.toString().substring(s.toString().length() - 1).equals("@")) {
                        Toast.makeText(context, "Open mention list", Toast.LENGTH_SHORT).show();
                    }
                }*/
                if (user_list != null) {
                    if (user_list.size() > 0) {
                        int counter = 0;
                        if (s.toString().length() > 0) {
                            if (s.toString().length() == 1 && String.valueOf(s.toString().charAt(s.toString().length() - 1)).equals("@")) {
                                //Toast.makeText(context, "Open mention list", Toast.LENGTH_SHORT).show();
                                text_inputted = s.toString();
                                setAdapter1();
                            } else if (s.toString().length() > 1) {

                                if (String.valueOf(s.toString().charAt(s.toString().length() - 1)).equals("@") && String.valueOf(s.toString().charAt(s.toString().length() - 2)).equals(" ")) {
                                   // Toast.makeText(context, "Open mention list", Toast.LENGTH_SHORT).show();
                                    text_inputted = s.toString();
                                    setAdapter1();

                                }

                                for (int i = 0; i < s.toString().length(); i++) {
                                    if (s.charAt(i) == '@') {
                                        counter++;
                                        Log.e("Gone_list:", "3");
                                    }
                                }
                                Log.e("counter", String.valueOf(counter));
                                if (counter > 0) {
                                    if (mentionUserAdapter != null)
                                        mentionUserAdapter.filter(typeMessage.getText().toString().substring(s.toString().lastIndexOf("@") + 1, typeMessage.getText().toString().length()));
                                } else {
                                    setAdapter_comment();
                                }
                            }
                        } else {
                            setAdapter_comment();
                        }
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void setAdapter_comment() {
        Adapter = new CommentAdapter(context, list);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(Adapter);
        myRecyclerView.scrollToPosition(Adapter.getItemCount() - 1);

    }

    private void setAdapter1() {
        mentionUserAdapter = new MentionUserAdapter(context, user_list);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(mentionUserAdapter);

    }

    public void setclickdata(String text) {
        String text_inputted_new = text_inputted.substring(0, text_inputted.lastIndexOf("@"));
        typeMessage.setText(text_inputted_new + "@" + text.replace(" ", "") + " ");
        typeMessage.setSelection(typeMessage.getText().length());

        Adapter = new CommentAdapter(context, list);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(Adapter);
        myRecyclerView.scrollToPosition(Adapter.getItemCount() - 1);


    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Comments");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    private void GET_COMMENTAPI() {
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.POST_ID, "200");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.COMMENT + "?post_id=" + post_id, formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (list.size() > 0) {
                    list.clear();
                }
                if (ConnectivityReceiver.isConnected()) {
                    USER_LISTING();
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                typeMessage.setText("");
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject data = jsonmainObject.getJSONObject("data");
                            JSONArray body = data.getJSONArray("result");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                CommentModel commentModel = new CommentModel();
                                commentModel.setComment(object.getString("comment"));
                                //commentModel.setUser_type(object.optString("user_type"));
                                // commentModel.setVideo(object.optString("video"));
                                commentModel.setType(object.optString("comment_type"));
                                commentModel.setThumb(object.optString("thumb_image"));
                                commentModel.setTotal_likes(object.optString("total_likes"));
                                commentModel.setIs_like(object.optString("is_like"));
                                commentModel.setCreated(object.getString("created"));
                                commentModel.setId(object.getString("id"));
                                commentModel.setName(object.getString("first_name"));
                                commentModel.setImage(object.getString("profile"));
                                commentModel.setUser_id(object.getString("user_id"));
                                commentModel.setVerfiy_badge(object.optString("verfiy_badge"));
                                list.add(commentModel);
                            }
                            if (list.size() > 0) {
                                Collections.reverse(list);
                                Adapter = new CommentAdapter(context, list);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(Adapter);
                                myRecyclerView.scrollToPosition(Adapter.getItemCount() - 1);
                            }

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void POST_COMMENT(String type) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);


        if (type.equals("1")) {
            if (!selectedimage.isEmpty()) {
                MediaType MEDIA_TYPE = null;
                MEDIA_TYPE = selectedimage.endsWith("mp4") ? MediaType.parse("/mp4") : MediaType.parse("/mp4");
                File file = new File(selectedimage);
                formBuilder.addFormDataPart(Parameters.COMMENT, file.getName(), RequestBody.create(MEDIA_TYPE, file));
            }

            if (!thumb1.isEmpty()) {
                final MediaType MEDIA_TYPE = thumb1.endsWith("png") ?
                        MediaType.parse("image/png") : MediaType.parse("image/jpeg");

                File file = new File(thumb1);
                formBuilder.addFormDataPart(Parameters.THUMB_IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
            }
        } else {
            formBuilder.addFormDataPart(Parameters.COMMENT, typeMessage.getText().toString().trim());
            formBuilder.addFormDataPart(Parameters.THUMB_IMAGE, "abc");
        }
        formBuilder.addFormDataPart(Parameters.COMMENT_TYPE, type);
        formBuilder.addFormDataPart(Parameters.POST_ID, post_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.COMMENT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                typeMessage.setText("");

                util.hideKeyboard(context);
                mDialog.dismiss();
                selectedimage = "";
                thumb1 = "";
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject object = jsonmainObject.getJSONObject("data");
                            CommentModel commentModel = new CommentModel();

                            //commentModel.setVideo(object.optString("video"));
                            commentModel.setType(object.optString("comment_type"));
                            commentModel.setThumb(object.optString("thumb_image"));
                            commentModel.setTotal_likes("0");
                            commentModel.setIs_like("0");
                            commentModel.setComment(object.getString("comment"));
                            commentModel.setCreated(object.getString("created"));
                            commentModel.setId(object.getString("id"));
                            commentModel.setImage(savePref.getImage());
                            commentModel.setName(savePref.getFirstName() + " " + savePref.getLastName());
                            commentModel.setUser_id(object.getString("user_id"));
                            list.add(commentModel);

                            Adapter = new CommentAdapter(context, list);
                            myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                            myRecyclerView.setAdapter(Adapter);
                            myRecyclerView.scrollToPosition(Adapter.getItemCount() - 1);
                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void USER_LISTING() {
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, savePref.getID());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.FOLLOWING + "?follower=0&limit=100000", formBody) {
            @Override
            public void getValueParse(String result) {
                user_list = new ArrayList<>();
                if (user_list.size() > 0)
                    user_list.clear();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONObject("data").getJSONArray("result");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                UserModel userModel = new UserModel();
                                userModel.setEmail(object.getString("email"));
                                userModel.setFirst_name(object.getString("first_name"));
                                userModel.setLast_name(object.getString("last_name"));
                                userModel.setId(object.getString("id"));
                                userModel.setVerfiy_badge(object.optString("verfiy_badge"));
                                userModel.setProfile(object.getString("profile"));
                                user_list.add(userModel);
                            }


                        } else {
                            util.showToast(context, jsonmainObject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void COMMENT_LIKE(int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.COMMENT_ID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.COMMENT_LIKE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                util.hideKeyboard(context);
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {


                            if (list.get(position).getIs_like().equals("1")) {
                                list.get(position).setTotal_likes(String.valueOf(Integer.valueOf(list.get(position).getTotal_likes()) - 1));
                                list.get(position).setIs_like("0");
                            } else {
                                list.get(position).setTotal_likes(String.valueOf(Integer.valueOf(list.get(position).getTotal_likes()) + 1));
                                list.get(position).setIs_like("1");
                            }

                            Adapter = new CommentAdapter(context, list);
                            myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                            myRecyclerView.setAdapter(Adapter);
                            myRecyclerView.scrollToPosition(position);
                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void DeleteAlert(int position) {
        new IOSDialog.Builder(context)
                .setCancelable(false)
                .setMessage("Are you sure to Delete Comment?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DELETE_COMMENT_API(position);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void DELETE_COMMENT_API(int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.POST_ID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncDELETE mAsync = new GetAsyncDELETE(context, AllAPIS.COMMENT + "?comment_id=" + list.get(position).getId(), formBody, mDialog, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(Response response) {
                mDialog.dismiss();

                String result = response.body().toString();
                if (response.isSuccessful()) {
                    new IOSDialog.Builder(context)
                            .setCancelable(false)
                            .setMessage("Comment Deleted Successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            list.remove(position);
                            Adapter.notifyDataSetChanged();
                        }
                    }).show();
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @OnClick({R.id.attachment, R.id.send_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.attachment:
                Intent intent = new Intent(android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
                intent.putExtra("android.intent.extra.durationLimit", 7);//limit cmaera to capture limited video
                startActivityForResult(intent, VIDEO_CAPTURE);
                break;
            case R.id.send_button:
                if (!typeMessage.getText().toString().trim().isEmpty())
                    POST_COMMENT("0");
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == VIDEO_CAPTURE) {
                selectedimage = getAbsolutePath(this, data.getData());
                Bitmap video_bitmap = ThumbnailUtils.createVideoThumbnail(selectedimage, MediaStore.Video.Thumbnails.MINI_KIND);
                thumb1 = getInsertedPath(video_bitmap, "");
                POST_COMMENT("1");


            }

        }

    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public String getInsertedPath(Bitmap bitmap, String path) {
        String strMyImagePath = null;
        File mFolder = new File(context.getExternalCacheDir() + "/2udezz");

        if (!mFolder.exists()) {
            mFolder.mkdir();
        }

        String s = String.valueOf(System.currentTimeMillis() / 1000) + ".jpg";
        File f = new File(mFolder.getAbsolutePath(), s);
        Log.e("creating", "name " + f.getName());
        strMyImagePath = f.getAbsolutePath();
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (strMyImagePath == null) {
            return path;
        }
        return strMyImagePath;
    }

    private void bitmap(Uri resultUri) {
        Bitmap bitmap = null;
        try {
            bitmap = (MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
