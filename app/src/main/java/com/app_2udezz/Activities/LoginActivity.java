package com.app_2udezz.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app_2udezz.MainActivity;
import com.app_2udezz.R;
import com.app_2udezz.Util.ConnectivityReceiver;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class LoginActivity extends AppCompatActivity {
    LoginActivity context;
    @BindView(R.id.bio)
    EditText bio;
    @BindView(R.id.bio_layout)
    LinearLayout bioLayout;
    @BindView(R.id.term_check_text)
    TextView termCheckText;
    @BindView(R.id.term_check_lay)
    LinearLayout termCheckLay;
    @BindView(R.id.privacy)
    CheckBox privacy;
    @BindView(R.id.privacy_text)
    TextView privacyText;
    @BindView(R.id.privacy_lay)
    LinearLayout privacyLay;
    @BindView(R.id.code)
    TextView code;
    @BindView(R.id.phone_view)
    View phoneView;
    private SavePref savePref;
    @BindView(R.id.profile_pic)
    ImageView profilePic;
    @BindView(R.id.first_name)
    EditText firstName;
    @BindView(R.id.first_name_layout)
    LinearLayout firstNameLayout;
    @BindView(R.id.last_name)
    EditText lastName;
    @BindView(R.id.last_name_layout)
    LinearLayout lastNameLayout;
    @BindView(R.id.email_address)
    EditText emailAddress;
    @BindView(R.id.email_address_layout)
    LinearLayout emailAddressLayout;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.phone_layout)
    LinearLayout phoneLayout;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.term_check)
    CheckBox termCheck;
    @BindView(R.id.forgot_password)
    Button forgotPassword;
    @BindView(R.id.next_button)
    ImageView nextButton;

    boolean is_signin = true;
    @BindView(R.id.login)
    TextView login;
    @BindView(R.id.sign_up)
    TextView signUp;

    private String selectedimage = "";
    Uri fileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        context = LoginActivity.this;
        savePref = new SavePref(context);

        privacyText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent11 = new Intent(context, TermConditionActivity.class);
                intent11.putExtra("type", "about");
                startActivity(intent11);
            }
        });
        termCheckText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(context, TermConditionActivity.class);
                intent2.putExtra("type", "term");
                startActivity(intent2);
            }
        });

    }


    @OnClick({R.id.profile_pic, R.id.login, R.id.sign_up, R.id.code, R.id.forgot_password, R.id.next_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profile_pic:
                CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            case R.id.login:
                emailAddress.requestFocus();
                is_signin = true;
                login.setTextColor(getResources().getColor(R.color.white));
                signUp.setTextColor(getResources().getColor(R.color.light_gray));
                firstNameLayout.setVisibility(View.GONE);
                lastNameLayout.setVisibility(View.GONE);
                phoneLayout.setVisibility(View.GONE);
                phoneView.setVisibility(View.GONE);
                bioLayout.setVisibility(View.GONE);
                termCheckLay.setVisibility(View.GONE);
                privacyLay.setVisibility(View.GONE);
                forgotPassword.setVisibility(View.VISIBLE);
                Glide.with(context).load(R.drawable.logo).into(profilePic);

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) emailAddressLayout.getLayoutParams();
                params.setMargins(0, 80, 0, 0);
                emailAddressLayout.setLayoutParams(params);

                break;
            case R.id.sign_up:
                firstName.requestFocus();
                is_signin = false;
                signUp.setTextColor(getResources().getColor(R.color.white));
                login.setTextColor(getResources().getColor(R.color.light_gray));
                firstNameLayout.setVisibility(View.VISIBLE);
                //lastNameLayout.setVisibility(View.VISIBLE);

                phoneView.setVisibility(View.VISIBLE);
                phoneLayout.setVisibility(View.VISIBLE);
                bioLayout.setVisibility(View.VISIBLE);
                termCheckLay.setVisibility(View.VISIBLE);
                privacyLay.setVisibility(View.VISIBLE);
                forgotPassword.setVisibility(View.GONE);

                Glide.with(context).load(R.drawable.a5).into(profilePic);

                LinearLayout.LayoutParams par = (LinearLayout.LayoutParams) emailAddressLayout.getLayoutParams();
                par.setMargins(0, 10, 0, 0);
                emailAddressLayout.setLayoutParams(par);


                break;
            case R.id.code:
                CountryPicker picker = CountryPicker.newInstance("Select Country Code");  // dialog title
                picker.setListener(new CountryPickerListener() {
                    @Override
                    public void onSelectCountry(String name, String code_, String dialCode, int flagDrawableResID) {
                        picker.dismiss();
                        code.setText(dialCode);
                    }
                });
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;
            case R.id.forgot_password:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.next_button:
                if (is_signin) {
                    SigninProcess();
                } else {
                    SignUpProcess();
                }
                break;
        }
    }

    private void SigninProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (emailAddress.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Email Address");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter a Vaild Email Address");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (password.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Password");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                LOGIN_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void LOGIN_API() {
        util.hideKeyboard(context);
        //Log.e("device_token__", SavePref.getDeviceToken(LoginActivity.this, "token"));
        //String s = SavePref.getDeviceToken(LoginActivity.this, "token");
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USERLOGIN, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("data");
                            savePref.setAuthorization_key(body.getString("authorization_key"));
                            savePref.setID(body.getString("id"));
                            savePref.setEmail(body.getString("email"));
                            savePref.setFirstName(body.getString("first_name"));
                            savePref.setLastName(body.getString("last_name"));
                            savePref.setPhone(body.optString("phone_code") + body.getString("phone"));
                            savePref.setImage(body.getString("profile"));
                            savePref.setStringLatest("about_us", body.getString("about_us"));
                            savePref.setStringLatest("delete_time", body.getString("delete_time"));
                            savePref.setStringLatest("audio", body.getString("audio"));
                            savePref.setStringLatest("is_private", body.getString("is_private"));
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            util.showToast(context, "Login Sucessfully!!!");
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void SignUpProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (selectedimage.isEmpty()) {
                util.IOSDialog(context, "Please Select Profile Picture");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (firstName.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Your User Name");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } /*else if (lastName.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Your Last Name");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } */ else if (emailAddress.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Email Address");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter a Vaild Email Address");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (phone.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Phone");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (bio.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Bio");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (password.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Password");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!termCheck.isChecked()) {
                util.IOSDialog(context, "Please Accept Terms & Conditions");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!privacy.isChecked()) {
                util.IOSDialog(context, "Please Accept Privacy Policy");
                nextButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                Intent intent = new Intent(context, AudioUploadActivity.class);
                intent.putExtra("selectedimage", selectedimage);
                intent.putExtra("firstName", firstName.getText().toString().trim());
                intent.putExtra("lastName", lastName.getText().toString().trim());
                intent.putExtra("emailAddress", emailAddress.getText().toString().trim());
                intent.putExtra("phone", phone.getText().toString().trim());
                intent.putExtra("bio", bio.getText().toString().trim());
                intent.putExtra("password", password.getText().toString().trim());
                intent.putExtra("code", code.getText().toString().trim().substring(1));
                startActivity(intent);
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                selectedimage = getAbsolutePath(this, resultUri);
                Glide.with(context).load(selectedimage).into(profilePic);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
}
