package com.app_2udezz.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;

import com.app_2udezz.R;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AccountPrivacyActivity extends AppCompatActivity {
    AccountPrivacyActivity context;
    @BindView(R.id.switch___)
    SwitchCompat switch___;
    private SavePref savePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_account_privacy);
        ButterKnife.bind(this);
        context = AccountPrivacyActivity.this;
        savePref = new SavePref(context);
        if (savePref.getStringLatest("is_private").equals("1")) {
            switch___.setChecked(true);
        } else {
            switch___.setChecked(false);
        }


        switch___.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    EDIT_PROFILE_API("1");
                else
                    EDIT_PROFILE_API("0");
            }
        });
        setToolbar();
    }

    private void EDIT_PROFILE_API(String type) {
        Log.e("type___", type);
        util.hideKeyboard(context);
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.IS_PRIVATE, type);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.EDIT_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, "Setting Updated Successfully!");
                            savePref.setStringLatest("is_private", type);
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.account_privacy));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
