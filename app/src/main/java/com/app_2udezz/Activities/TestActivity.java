package com.app_2udezz.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Adapters.HomeAdapter;
import com.app_2udezz.Adapters.HomeTestAdapter;
import com.app_2udezz.DataModels.PostModel;
import com.app_2udezz.Fragments.HomeFragment;
import com.app_2udezz.MainActivity;
import com.app_2udezz.R;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class TestActivity extends AppCompatActivity {
    TestActivity context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    ProgressDialog mDialog;
    private ArrayList<PostModel> list = new ArrayList<>();
    private String offset = "1";
    private int total_post = 0;
    LinearLayoutManager linearLayoutManager;

    private SavePref savePref;
    int pos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ButterKnife.bind(this);

        context = TestActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);

        linearLayoutManager = new LinearLayoutManager(context);
        myRecyclerView.setLayoutManager(linearLayoutManager);

        POSTS();

        LoadmoreElements();
    }

    private void LoadmoreElements() {
        myRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (linearLayoutManager.findFirstCompletelyVisibleItemPosition()
                        == 0) {
                    if (list.size() == total_post) {
                        Toast.makeText(context, "No More Posts to Display", Toast.LENGTH_SHORT).show();
                    } else {
                        pos = 0;
                        pos = list.size();
                        //Toast.makeText(context, "Loading more...", Toast.LENGTH_SHORT).show();
                        offset = String.valueOf(Integer.valueOf(offset) + 1);
                        POSTS();
                    }
                }


            }
        });


    }

    private void POSTS() {
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.POSTS + "/" + offset, formBody) {
            @Override
            public void getValueParse(String result) {
                if (list.size() > 0 && offset.equals("1")) {
                    list = new ArrayList<>();
                    list.clear();
                }
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONObject("data").getJSONArray("result");

                            total_post = Integer.parseInt(jsonmainObject.getJSONObject("data").getJSONObject("pagination").getString("totalRecord"));

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                PostModel postModel = new PostModel();
                                postModel.setCreated(object.getString("created"));
                                postModel.setDescription(object.getString("description"));
                                postModel.setId(object.getString("id"));
                                postModel.setIs_like(object.getString("is_like"));
                                postModel.setLocations(object.getString("locations"));
                                postModel.setMedia(object.getString("media"));
                                postModel.setPost_type(object.getString("post_type"));
                                postModel.setTitle(object.getString("title"));
                                postModel.setTotal_comments(object.getString("total_comments"));
                                postModel.setTotal_likes(object.getString("total_likes"));
                                postModel.setUser_id(object.getString("user_id"));
                                postModel.setVerfiy_badge(object.optString("verfiy_badge"));

                                if (object.getString("user_id").equals(savePref.getID())) {
                                    postModel.setIs_show_delete(true);
                                } else {
                                    postModel.setIs_show_delete(false);
                                }


                                postModel.setMedia_type(object.optString("media_type"));

                                postModel.setImage(object.getString("profile"));
                                postModel.setName(object.getString("first_name"));
                                list.add(0, postModel);
                            }

                            if (list.size() > 0) {
                                HomeTestAdapter adapter = new HomeTestAdapter(context, list);
                                /*if (Integer.valueOf(offset) > 1)
                                    myRecyclerView.scrollToPosition(linearLayoutManager.findFirstVisibleItemPosition());
                                else {
                                    if (offset.equals("1"))
                                        myRecyclerView.scrollToPosition(adapter.getItemCount() - 1);

                                }*/

                                myRecyclerView.setAdapter(adapter);

                                if (offset.equals("1"))
                                    myRecyclerView.scrollToPosition(adapter.getItemCount() - 1);
                                else {
                                    Log.e("position____", String.valueOf(pos));
                                    int pos_new = pos - 1;

                                    myRecyclerView.scrollToPosition(pos_new);


                                }


                            }

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

}
