package com.app_2udezz.DataModels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class PostModel implements Parcelable {
    String id = "";
    String title = "";
    String description = "";
    String post_type = "";
    String media = "";
    String total_likes = "";
    String total_comments = "";
    String is_like = "";
    String created = "";
    String locations = "";
    String user_id = "";
    String image = "";
    String name = "";
    String verfiy_badge = "";
    String media_type = "";
    boolean is_show_delete = false;
    ArrayList<StoryModel>list;

    public PostModel() {
    }

    protected PostModel(Parcel in) {
        id = in.readString();
        title = in.readString();
        description = in.readString();
        post_type = in.readString();
        media = in.readString();
        total_likes = in.readString();
        total_comments = in.readString();
        is_like = in.readString();
        created = in.readString();
        locations = in.readString();
        user_id = in.readString();
        image = in.readString();
        name = in.readString();
        verfiy_badge = in.readString();
        media_type = in.readString();
        is_show_delete = in.readByte() != 0;
        list = in.createTypedArrayList(StoryModel.CREATOR);
    }

    public static final Creator<PostModel> CREATOR = new Creator<PostModel>() {
        @Override
        public PostModel createFromParcel(Parcel in) {
            return new PostModel(in);
        }

        @Override
        public PostModel[] newArray(int size) {
            return new PostModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getTotal_likes() {
        return total_likes;
    }

    public void setTotal_likes(String total_likes) {
        this.total_likes = total_likes;
    }

    public String getTotal_comments() {
        return total_comments;
    }

    public void setTotal_comments(String total_comments) {
        this.total_comments = total_comments;
    }

    public String getIs_like() {
        return is_like;
    }

    public void setIs_like(String is_like) {
        this.is_like = is_like;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVerfiy_badge() {
        return verfiy_badge;
    }

    public void setVerfiy_badge(String verfiy_badge) {
        this.verfiy_badge = verfiy_badge;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public boolean isIs_show_delete() {
        return is_show_delete;
    }

    public void setIs_show_delete(boolean is_show_delete) {
        this.is_show_delete = is_show_delete;
    }

    public ArrayList<StoryModel> getList() {
        return list;
    }

    public void setList(ArrayList<StoryModel> list) {
        this.list = list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(post_type);
        dest.writeString(media);
        dest.writeString(total_likes);
        dest.writeString(total_comments);
        dest.writeString(is_like);
        dest.writeString(created);
        dest.writeString(locations);
        dest.writeString(user_id);
        dest.writeString(image);
        dest.writeString(name);
        dest.writeString(verfiy_badge);
        dest.writeString(media_type);
        dest.writeByte((byte) (is_show_delete ? 1 : 0));
        dest.writeTypedList(list);
    }
}
