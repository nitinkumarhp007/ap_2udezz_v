package com.app_2udezz.DataModels;

import android.graphics.Bitmap;

public class FilterModel {
    Bitmap bitmap;
    boolean is_select=false;


    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public boolean isIs_select() {
        return is_select;
    }

    public void setIs_select(boolean is_select) {
        this.is_select = is_select;
    }
}
