package com.app_2udezz.DataModels;

public class UserModel {

    String id="";
    String first_name="";
    String last_name="";
    String email="";
    String profile="";
    String verfiy_badge="";

    public String getVerfiy_badge() {
        return verfiy_badge;
    }

    public void setVerfiy_badge(String verfiy_badge) {
        this.verfiy_badge = verfiy_badge;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }
}
