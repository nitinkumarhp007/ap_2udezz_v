package com.app_2udezz.DataModels;

import android.os.Parcel;
import android.os.Parcelable;

public class StoryModel implements Parcelable {
    String id="";
    String user_id="";
    String post_type="";
    String post="";
    String totalViews="";
    String thumbImage="";
    String image="";
    String name="";

    public StoryModel()
    {}

    protected StoryModel(Parcel in) {
        id = in.readString();
        user_id = in.readString();
        post_type = in.readString();
        post = in.readString();
        totalViews = in.readString();
        thumbImage = in.readString();
        image = in.readString();
        name = in.readString();
    }

    public static final Creator<StoryModel> CREATOR = new Creator<StoryModel>() {
        @Override
        public StoryModel createFromParcel(Parcel in) {
            return new StoryModel(in);
        }

        @Override
        public StoryModel[] newArray(int size) {
            return new StoryModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(String totalViews) {
        this.totalViews = totalViews;
    }

    public String getThumbImage() {
        return thumbImage;
    }

    public void setThumbImage(String thumbImage) {
        this.thumbImage = thumbImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(user_id);
        dest.writeString(post_type);
        dest.writeString(post);
        dest.writeString(totalViews);
        dest.writeString(thumbImage);
        dest.writeString(image);
        dest.writeString(name);
    }
}
