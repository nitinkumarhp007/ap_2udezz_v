package com.app_2udezz.DataModels;

public class NotificationsModel {

    String id = "";
    String text = "";
    String profile = "";
    String user_id = "";
    String created = "";
    String user_type = "";
    String type = "";
    String verfiy_badge = "";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getVerfiy_badge() {
        return verfiy_badge;
    }

    public void setVerfiy_badge(String verfiy_badge) {
        this.verfiy_badge = verfiy_badge;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
