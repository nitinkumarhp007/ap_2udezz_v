package com.app_2udezz.parser;


public class AllAPIS {

    public static final String BASE_URL = "http://3.12.247.224/apis/v1/";

    public static final String USERLOGIN = BASE_URL + "user/login";
    public static final String USER_SIGNUP = BASE_URL + "user";
    public static final String VERIFY_OTP = BASE_URL + "user/verify";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgot-password";
    public static final String LOGOUT = BASE_URL + "logout";
    public static final String CHANGEPASSWORD = BASE_URL + "change_password";
    public static final String EDIT_PROFILE = BASE_URL + "user/edit";
    public static final String APP_INFO = BASE_URL + "app-information";

    public static final String POSTS = BASE_URL + "posts";
    public static final String POST_TAGS = BASE_URL + "post/tags";
    public static final String POSTS_TRENDING = BASE_URL + "posts-trending";
    public static final String COMMENT = BASE_URL + "comment";
    public static final String POST_LIKE = BASE_URL + "post/like";
    public static final String POST_DISLIKE = BASE_URL + "post/dislike";
    public static final String FOLLOW = BASE_URL + "follow";
    public static final String FOLLOWING = BASE_URL + "following";
    public static final String USER_DETAIL = BASE_URL + "user";
    public static final String BADGE_REQUEST = BASE_URL + "badge-request";
    public static final String LAST_CHAT = BASE_URL + "last-chat";
    public static final String SEND_MESSAGE = BASE_URL + "send-message";
    public static final String GET_MESSAGE = BASE_URL + "get-message";
    public static final String NOTIFICATIONS = BASE_URL + "notifications";
    public static final String POST_DETAIL = BASE_URL + "post-detail";
    public static final String DELETE_THREAD = BASE_URL + "delete-thread";
    public static final String DELETE_MESSAGE = BASE_URL + "delete-message";
    public static final String DELETE_POST = BASE_URL + "posts";
    public static final String USER_BLOCK = BASE_URL + "user/block";
    public static final String POST_REPORT = BASE_URL + "post-report";
    public static final String USER_LISTING = BASE_URL + "user-listing";
    public static final String COMMENT_LIKE = BASE_URL + "comment-like";
    public static final String RANDOM_POSTS = BASE_URL + "post/home";

    public static final String ADD_STORY = BASE_URL + "story/add";
    public static final String MY_STORIES = BASE_URL + "story/my";
    public static final String REMOVE_STORY = BASE_URL + "story/remove";
    public static final String VIEW_STORY = BASE_URL + "story/view";
    public static final String ALL_STORIES = BASE_URL + "stories";
    public static final String STORY_REPLY = BASE_URL + "story-reply";
}
