package com.app_2udezz.Util;

public class Parameters {
    public static final String AUTHORIZATION_KEY = "authorization_key";
    public static final String AUTH_KEY = "auth_key";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String EMAIL = "email";
    public static final String SOCIAL_ID = "social_id";
    public static final String SOCIAL_TYPE = "soical_type";
    public static final String OTP = "otp";
    public static final String DEVICE_TYPE = "device_type";
    public static final String DELETE_TIME = "delete_time";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String PHONE = "phone";
    public static final String ABOUT_US = "about_us";
    public static final String COUNTRY_CODE = "country_code";
    public static final String COUNTRY = "country";
    public static final String OLD_PASSWORD = "old_password";
    public static final String NEW_PASSWORD = "new_password";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String PROFILE = "profile";
    public static final String PHONE_CODE = "phone_code";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String LOCATIONS = "locations";
    public static final String MEDIA = "media";
    public static final String COMMENT_TYPE = "comment_type";
    public static final String THUMB_IMAGE = "thumb_image";
    public static final String COMMENT = "comment";
    public static final String POST_ID = "post_id";

    public static final String FRIEND_ID = "friend_id";
    public static final String DOCUMENT = "document";
    public static final String MESSAGE = "message";
    public static final String MESSAGE_TYPE = "message_type";
    public static final String IMAGE = "image";
    public static final String VIDEO = "video";
    public static final String THREAD_ID = "thread_id";
    public static final String CHAT_ID = "chat_id";
    public static final String MEDIA_TYPE = "media_type";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String AUDIO = "audio";
    public static final String ID ="id" ;
    public static final String IS_PRIVATE ="is_private" ;
    public static final String COMMENT_ID = "comment_id";
    public static final String THUMBIMAGE = "thumbImage";
    public static final String POST_TYPE = "post_type";
    public static final String POST = "post";
    public static final String STORY_ID = "story_id";
    public static final String USER_ID = "user_id";
}
















