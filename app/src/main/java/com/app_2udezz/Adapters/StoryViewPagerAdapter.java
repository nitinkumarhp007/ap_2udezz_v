package com.app_2udezz.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.app_2udezz.Activities.StoryViewActivity;
import com.app_2udezz.DataModels.PostModel;
import com.app_2udezz.DataModels.StoryModel;
import com.app_2udezz.R;
import com.app_2udezz.Util.ConnectivityReceiver;
import com.app_2udezz.Util.KeyboardUtils;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import jp.shts.android.storiesprogressview.StoriesProgressView;

public class StoryViewPagerAdapter extends PagerAdapter implements StoriesProgressView.StoriesListener {
    LayoutInflater inflater;
    StoryViewActivity context;
    private SavePref savePref;
    int count = 0;
    public StoriesProgressView storiesProgressView;
    ProgressBar progressBar;
    EditText typeMessage;
    RelativeLayout write_layout;
    View v1, v2;
    ImageView image, videoPlay, sendButton;
    //ArrayList<StoryModel> list;
    ArrayList<PostModel> list_post;

    public StoryViewPagerAdapter(StoryViewActivity addDetailActivity, ArrayList<PostModel> list_post, int position) {
        this.context = addDetailActivity;
        this.list_post = list_post;
        savePref = new SavePref(context);
    }

    @Override
    public int getCount() {
        return list_post.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        Log.e("position____viewpager", String.valueOf(position));

        // po = position;


        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemview = inflater.inflate(R.layout.story_viewpager_layout, container, false);


        image = (ImageView) itemview.findViewById(R.id.image);
        write_layout = (RelativeLayout) itemview.findViewById(R.id.write_layout);
        storiesProgressView = (StoriesProgressView) itemview.findViewById(R.id.stories);
        progressBar = (ProgressBar) itemview.findViewById(R.id.progress_bar);
        videoPlay = (ImageView) itemview.findViewById(R.id.video_play);
        typeMessage = (EditText) itemview.findViewById(R.id.type_message);
        sendButton = (ImageView) itemview.findViewById(R.id.send_button);
        v1 = (View) itemview.findViewById(R.id.v1);
        v2 = (View) itemview.findViewById(R.id.v2);
        progressBar.setVisibility(View.GONE);


        //list = list_post.get(position).getList();

        if (list_post.get(context.position_item).getUser_id().equals(savePref.getID())) {
            write_layout.setVisibility(View.GONE);
        } else {
            write_layout.setVisibility(View.VISIBLE);
        }


        setImage(count);

        storiesProgressView.setStoriesCount(list_post.get(context.position_item).getList().size()); // <- set stories
        storiesProgressView.setStoryDuration(7000); // <- set a story duration
        storiesProgressView.setStoriesListener(this); // <- set listener
        storiesProgressView.startStories(); // <- start progress


        KeyboardUtils.addKeyboardToggleListener(context, new KeyboardUtils.SoftKeyboardToggleListener() {
            @Override
            public void onToggleSoftKeyboard(boolean isVisible) {
                Log.d("keyboard", "keyboard visible: " + isVisible);

                if (isVisible) {
                    storiesProgressView.pause();
                } else {
                    storiesProgressView.resume();
                }

            }
        });

        v2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storiesProgressView.skip();
                Log.e("count_numer", String.valueOf(count));
            }
        });
        v1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storiesProgressView.reverse();
                Log.e("count_numer", String.valueOf(count));
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    if (!typeMessage.getText().toString().isEmpty()) {
                        String comment = typeMessage.getText().toString().trim();
                        typeMessage.setText("");
                        context.POST_COMMENT(comment,
                                list_post.get(context.position_item).getList().get(position).getId(), list_post.get(context.position_item).getList().get(position).getUser_id());
                    }

                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
            }
        });

        //add item.xml to viewpager
        ((ViewPager) container).addView(itemview);
        return itemview;
    }

    private void setImage(int position) {
        if (list_post.get(context.position_item).getList().size() > position) {
            Log.e("image__", list_post.get(context.position_item).getList().get(position).getPost());
            Glide.with(context)
                    .load(list_post.get(context.position_item).getList().get(position).getPost())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(image);
        }
        //Glide.with(context).load(list.get(position).getPost()).into(image);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object);
    }

    @Override
    public void onNext() {
        // Toast.makeText(this, "onNext", Toast.LENGTH_SHORT).show();

        Log.e("calllll", "onNext " + String.valueOf(count));

        typeMessage.setText("");

        count = count + 1;

        if (list_post.get(context.position_item).getList().size() == (count)) {
            count = 0;
            Log.e("calllll", "onNext viewpager" + String.valueOf(count));
            //context.viewpager.setCurrentItem(context.position_item + 1);
        } else {
            Log.e("calllll", "onNext setImage" + String.valueOf(count));
            setImage(count);
        }

        // Log.e("count_numer", String.valueOf(count));
    }

    @Override
    public void onPrev() {

        Log.e("calllll", "onPrev " + String.valueOf(count));
        typeMessage.setText("");
        if (count > 0) {
            Log.e("calllll", "onPrev setImage" + String.valueOf(count));
            count = count - 1;
            setImage(count);

        }
        // Log.e("count_numer", String.valueOf(count));
        // Call when finished revserse animation.
        // Toast.makeText(this, "onPrev", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onComplete() {
        Log.e("calllll", "onComplete " + String.valueOf(count));

        if (list_post.get(context.position_item).getList().size() == (count + 1)) {
            count = 0;
            Log.e("calllll", "onComplete viewpager" + String.valueOf(count));
            context.viewpager.setCurrentItem(context.position_item + 1);
        } else {
            // context.finish();
        }
        // Log.e("count_numer", String.valueOf(count));
       /* count = count - 1;
        setImage(count);*/
        //  Toast.makeText(this, "onComplete", Toast.LENGTH_SHORT).show();
    }


}