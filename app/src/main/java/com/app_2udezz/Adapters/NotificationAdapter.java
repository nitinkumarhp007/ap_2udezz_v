package com.app_2udezz.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Activities.FriendProfileActivity;
import com.app_2udezz.Activities.PostDetailActivity;
import com.app_2udezz.DataModels.NotificationsModel;
import com.app_2udezz.R;
import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<NotificationsModel> list;

    public NotificationAdapter(Context context, ArrayList<NotificationsModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.notification_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        holder.date.setText(getDate(Long.parseLong(list.get(position).getCreated())));
        holder.text.setText(list.get(position).getText());
        Glide.with(context).load(list.get(position).getProfile()).
                error(R.drawable.logo).into(holder.image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(list.get(position).getType().equals("4"))
                {
                    Intent intent = new Intent(context, FriendProfileActivity.class);
                    intent.putExtra("user_id", list.get(position).getUser_id());
                    context.startActivity(intent);
                }
                else {
                    Intent intent1 = new Intent(context, PostDetailActivity.class);
                    intent1.putExtra("is_from_push", false);
                    intent1.putExtra("post_id", list.get(position).getId());
                    context.startActivity(intent1);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    private String getDate(long time) {
        Date date = new Date(time*1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy "); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));

        return sdf.format(date);
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.date)
        TextView date;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
