package com.app_2udezz.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Activities.CommentsActivity;
import com.app_2udezz.Activities.FriendProfileActivity;
import com.app_2udezz.Activities.ImageShowActivity;
import com.app_2udezz.Activities.PostListingActivity;
import com.app_2udezz.Activities.ReportPostActivity;
import com.app_2udezz.DataModels.PostModel;
import com.app_2udezz.Fragments.HomeFragment;
import com.app_2udezz.R;
import com.app_2udezz.Util.util;
import com.appunite.appunitevideoplayer.PlayerActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.luseen.autolinklibrary.AutoLinkOnClickListener;
import com.luseen.autolinklibrary.AutoLinkTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;


public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;

    private View view;
    ArrayList<PostModel> list;
    HomeFragment homeFragment;

    public HomeAdapter(Context context, ArrayList<PostModel> list, HomeFragment homeFragment) {
        this.context = context;
        this.list = list;
        this.homeFragment = homeFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.home_post_items, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.description.addAutoLinkMode(AutoLinkMode.MODE_HASHTAG, AutoLinkMode.MODE_MENTION);
        holder.description.setHashtagModeColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        holder.description.setMentionModeColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        holder.description.setAutoLinkOnClickListener(new AutoLinkOnClickListener() {
            @Override
            public void onAutoLinkTextClick(AutoLinkMode autoLinkMode, String matchedText) {

                if (autoLinkMode == AutoLinkMode.MODE_MENTION) {

                  //  Toast.makeText(context, "Click on MODE_MENTION" + " " + matchedText.substring(1), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(context, FriendProfileActivity.class);
                    intent.putExtra("user_id", matchedText.substring(1));
                    context.startActivity(intent);



                } else if (autoLinkMode == AutoLinkMode.MODE_HASHTAG) {
                  //  Toast.makeText(context, "Click on MODE_HASHTAG" + " " + matchedText, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, PostListingActivity.class);
                    intent.putExtra("hashtag", matchedText.substring(1));
                    context.startActivity(intent);



                }

            }
        });


        if (list.get(position).isIs_show_delete()) {
            holder.option.setVisibility(View.GONE);
        } else {
            holder.option.setVisibility(View.VISIBLE);
        }

        if (list.get(position).getIs_like().equals("1"))
            holder.like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.h_r, 0, 0, 0);
        else
            holder.like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.h_b, 0, 0, 0);


        holder.timeDate.setText(getDate(Long.parseLong(list.get(position).getCreated())));

        holder.description.setAutoLinkText(list.get(position).getTitle());
        holder.like.setText(list.get(position).getTotal_likes());
        holder.Comment.setText(list.get(position).getTotal_comments());
        holder.location.setText(list.get(position).getLocations());
        holder.name.setText(list.get(position).getName());
        if (list.get(position).getVerfiy_badge().equals("1")) {
            holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.verified, 0);

        } else {
            holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        Glide.with(context).load(list.get(position).getImage()).into(holder.profilePic);

        Glide.with(context)
                .load(list.get(position).getMedia())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.image);

        holder.Comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CommentsActivity.class);
                intent.putExtra("post_id", list.get(position).getId());
                context.startActivity(intent);
            }
        });

        holder.videoPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
                        list.get(position).getMedia(),
                        " ", 0));
            }
        });


        if (list.get(position).getMedia_type().equals("1") || list.get(position).getMedia_type().equals("0")) {
            holder.videoPlay.setVisibility(View.GONE);
        } else {
            holder.videoPlay.setVisibility(View.VISIBLE);
        }

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getMedia_type().equals("1") || list.get(position).getMedia_type().equals("0")) {
                    Intent intent = new Intent(context, ImageShowActivity.class);
                    intent.putExtra("image", list.get(position).getMedia());
                    context.startActivity(intent);
                }
            }
        });

        holder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeFragment.LIKE_DISLIKE_POST(position, false);
            }
        });

        holder.profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FriendProfileActivity.class);
                intent.putExtra("user_id", list.get(position).getUser_id());
                context.startActivity(intent);
            }
        });
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FriendProfileActivity.class);
                intent.putExtra("user_id", list.get(position).getUser_id());
                context.startActivity(intent);
            }
        });

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                util.sharepost(list.get(position).getId(), context);
            }
        });
        holder.option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ReportPostActivity.class);
                intent.putExtra("post_id", list.get(position).getId());
                context.startActivity(intent);
            }
        });


    }

    private String getDate(long time) {
        Date date = new Date(time * 1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy "); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));

        return sdf.format(date);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.profile_pic)
        CircleImageView profilePic;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.location)
        TextView location;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.progress_bar)
        ProgressBar progressBar;
        @BindView(R.id.like)
        TextView like;
        @BindView(R.id.Comment)
        TextView Comment;
        @BindView(R.id.description)
        AutoLinkTextView description;
        @BindView(R.id.time_date)
        TextView timeDate;
        @BindView(R.id.video_play)
        ImageView videoPlay;
        @BindView(R.id.share)
        ImageView share;
        @BindView(R.id.option)
        ImageView option;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
