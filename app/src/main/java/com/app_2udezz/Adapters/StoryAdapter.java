package com.app_2udezz.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Activities.CommentsActivity;
import com.app_2udezz.Activities.FriendProfileActivity;
import com.app_2udezz.Activities.ImageShowActivity;
import com.app_2udezz.DataModels.PostModel;
import com.app_2udezz.Fragments.HomeFragment;
import com.app_2udezz.R;
import com.appunite.appunitevideoplayer.PlayerActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;


public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    HomeFragment homeFragment;
    ArrayList<PostModel> list;

    public StoryAdapter(Context context, ArrayList<PostModel> list_trending, HomeFragment homeFragment) {
        this.context = context;
        this.list = list_trending;
        this.homeFragment = homeFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.story_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.timeDate.setText(getDate(Long.parseLong(list.get(position).getCreated())));

       // holder.description.setText(list.get(position).getTitle());
        holder.like.setText(list.get(position).getTotal_likes());
        holder.Comment.setText(list.get(position).getTotal_comments());
       // holder.location.setText(list.get(position).getLocations());
        holder.name.setText(list.get(position).getName());
        if (list.get(position).getVerfiy_badge().equals("1")) {
            holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.verified, 0);

        } else {
            holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getMedia_type().equals("1") || list.get(position).getMedia_type().equals("0")) {
                    Intent intent = new Intent(context, ImageShowActivity.class);
                    intent.putExtra("image", list.get(position).getMedia());
                    context.startActivity(intent);
                }
            }
        });
        Glide.with(context)
                .load(list.get(position).getMedia())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.image);

        holder.Comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CommentsActivity.class);
                intent.putExtra("post_id", list.get(position).getId());
                context.startActivity(intent);
            }
        });

        holder.videoPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
                        list.get(position).getMedia(),
                        " ", 0));
            }
        });


        if (list.get(position).getMedia_type().equals("1") || list.get(position).getMedia_type().equals("0")) {
            holder.videoPlay.setVisibility(View.GONE);
        } else {
            holder.videoPlay.setVisibility(View.VISIBLE);
        }

        holder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeFragment.LIKE_DISLIKE_POST(position, true);
            }
        });

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FriendProfileActivity.class);
                intent.putExtra("user_id", list.get(position).getUser_id());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private String getDate(long time) {
        Date date = new Date(time * 1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy "); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));

        return sdf.format(date);
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.progress_bar)
        ProgressBar progressBar;
        @BindView(R.id.like)
        TextView like;
        @BindView(R.id.Comment)
        TextView Comment;
        @BindView(R.id.time_date)
        TextView timeDate;
        @BindView(R.id.video_play)
        ImageView videoPlay;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
