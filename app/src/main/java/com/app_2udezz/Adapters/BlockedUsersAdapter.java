package com.app_2udezz.Adapters;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Activities.FriendProfileActivity;
import com.app_2udezz.DataModels.UserModel;
import com.app_2udezz.R;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class BlockedUsersAdapter extends RecyclerView.Adapter<BlockedUsersAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<UserModel> list;
    ArrayList<UserModel> tempList;
    SavePref savePref;

    public BlockedUsersAdapter(Context context, ArrayList<UserModel> list) {
        this.context = context;
        this.list = list;
        this.tempList = list;
        savePref = new SavePref(context);
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.blocked_user_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        holder.name.setText(list.get(position).getFirst_name());

        Glide.with(context).load(list.get(position).getProfile()).into(holder.profilePic);


        holder.unblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Alert(position);
            }
        });
        if (list.get(position).getVerfiy_badge().equals("1")) {
            holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.verified, 0);

        } else {
            holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_pic)
        ImageView profilePic;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.unblock)
        TextView unblock;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    private void Alert(int position) {
        new IOSDialog.Builder(context)
                .setMessage("Are you sure to Unblock?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        USER_BLOCK(position);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void USER_BLOCK(int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.FRIEND_ID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USER_BLOCK, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, jsonMainobject.getString("message"));
                            list.remove(position);
                            notifyDataSetChanged();
                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<UserModel> nList = new ArrayList<UserModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (UserModel wp : tempList) {
                if ((wp.getFirst_name() + wp.getLast_name()).toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }


}
