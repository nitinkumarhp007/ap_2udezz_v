package com.app_2udezz.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Activities.ChattngActivity;
import com.app_2udezz.Activities.FriendProfileActivity;
import com.app_2udezz.DataModels.ChattingModel;
import com.app_2udezz.Fragments.ChatFragment;
import com.app_2udezz.R;
import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;


public class UserAdapter extends RecyclerView.Adapter<UserAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<ChattingModel> list;
    ChatFragment chatFragment;

    public UserAdapter(Context context, ArrayList<ChattingModel> list, ChatFragment chatFragment) {
        this.context = context;
        this.chatFragment = chatFragment;
        this.list = list;
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.user_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        String timestamp = list.get(position).getTimestamp(); //timestamp : 1254155422
        long timestampString = Long.parseLong(timestamp);
        String value = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").
                format(new Date(timestampString * 1000)); //convertion to 16/05/2017 17:33:42

        // holder.time.setText(value);
        Log.e("value", value);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = null;
        try {
            date = (Date) formatter.parse(value);//value:  16/05/2017 17:33:42
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String date_text = date.toString().substring(0, 10);

        holder.date.setText(date_text);

        holder.name.setText(list.get(position).getOpponent_name());
        holder.message.setText(list.get(position).getMessage());

        if (!list.get(position).getMessage().startsWith("http"))
            holder.message.setText(list.get(position).getMessage());
        else
            holder.message.setText("Image/Video");


        Glide.with(context).load(list.get(position).getImage()).into(holder.profilePic);

        holder.profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FriendProfileActivity.class);
                intent.putExtra("user_id", list.get(position).getUser_id());
                context.startActivity(intent);
            }
        });

        if (list.get(position).getVerfiy_badge().equals("1")) {
            holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.verified, 0);

        } else {
            holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChattngActivity.class);
                intent.putExtra("friend_id", list.get(position).getUser_id());
                intent.putExtra("friend_name", list.get(position).getOpponent_name());
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                chatFragment.DeleteAlert(position);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_pic)
        ImageView profilePic;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.message)
        TextView message;
        @BindView(R.id.date)
        TextView date;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}
