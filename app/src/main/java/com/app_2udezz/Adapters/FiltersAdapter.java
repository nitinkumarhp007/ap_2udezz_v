package com.app_2udezz.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.DataModels.FilterModel;
import com.app_2udezz.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    ArrayList<FilterModel> list;
    private View view;
    Bitmap bitmap;
    public FiltersAdapter(Context context, ArrayList<FilterModel> list, Bitmap bitmap) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.filters_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.image.setImageBitmap(bitmap);
    }


    @Override
    public int getItemCount() {
        return 3;
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
