package com.app_2udezz.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Activities.CommentsActivity;
import com.app_2udezz.Activities.FriendProfileActivity;
import com.app_2udezz.Activities.PostListingActivity;
import com.app_2udezz.DataModels.CommentModel;
import com.app_2udezz.R;
import com.app_2udezz.Util.SavePref;
import com.appunite.appunitevideoplayer.PlayerActivity;
import com.bumptech.glide.Glide;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.luseen.autolinklibrary.AutoLinkOnClickListener;
import com.luseen.autolinklibrary.AutoLinkTextView;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.RecyclerViewHolder> {
    CommentsActivity context;
    LayoutInflater Inflater;

    private View view;
    ArrayList<CommentModel> list;
    private SavePref savePref;

    public CommentAdapter(CommentsActivity context, ArrayList<CommentModel> list) {
        this.context = context;
        this.list = list;
        savePref = new SavePref(context);
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.comment_layout, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.comment.addAutoLinkMode(AutoLinkMode.MODE_HASHTAG, AutoLinkMode.MODE_MENTION);
        holder.comment.setHashtagModeColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        holder.comment.setMentionModeColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));


        if (list.get(position).getIs_like().equals("1")) {
            holder.like.setText(list.get(position).getTotal_likes() + " Unlike");
        } else {
            holder.like.setText(list.get(position).getTotal_likes() + " Like");
        }
        if (list.get(position).getType().equals("1")) {
            holder.comment.setVisibility(View.GONE);
            holder.videoLayout.setVisibility(View.VISIBLE);

            Glide.with(context).load(list.get(position).getThumb()).into(holder.video);


        } else {
            holder.videoLayout.setVisibility(View.GONE);
            holder.comment.setVisibility(View.VISIBLE);
            holder.comment.setAutoLinkText(list.get(position).getComment());
        }



        holder.comment.setAutoLinkOnClickListener(new AutoLinkOnClickListener() {
            @Override
            public void onAutoLinkTextClick(AutoLinkMode autoLinkMode, String matchedText) {

                if (autoLinkMode == AutoLinkMode.MODE_MENTION) {

                    // Toast.makeText(context, "Click on MODE_MENTION" + " " + matchedText.substring(1), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(context, FriendProfileActivity.class);
                    intent.putExtra("user_id", matchedText.substring(1));
                    context.startActivity(intent);



                } else if (autoLinkMode == AutoLinkMode.MODE_HASHTAG) {
                    Intent intent = new Intent(context, PostListingActivity.class);
                    intent.putExtra("hashtag", matchedText.substring(1));
                    context.startActivity(intent);
                    //  Toast.makeText(context, "Click on MODE_HASHTAG" + " " + matchedText, Toast.LENGTH_SHORT).show();
                }


            }
        });



        holder.name.setText(list.get(position).getName());
        Glide.with(context).load(list.get(position).getImage()).error(R.drawable.logo).into(holder.image);

        if (list.get(position).getVerfiy_badge().equals("1")) {
            holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.verified, 0);

        } else {
            holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        holder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.COMMENT_LIKE(position);
            }
        });


        holder.videoPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!list.get(position).getComment().isEmpty()) {
                    context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
                            list.get(position).getComment(),
                            " ", 0));
                }
            }
        });



        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FriendProfileActivity.class);
                intent.putExtra("user_id", list.get(position).getUser_id());
                context.startActivity(intent);
            }
        });
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FriendProfileActivity.class);
                intent.putExtra("user_id", list.get(position).getUser_id());
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (list.get(position).getUser_id().equals(savePref.getID())) {
                    context.DeleteAlert(position);
                }
                return false;
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.comment)
        AutoLinkTextView comment;
        @BindView(R.id.like)
        TextView like;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.video)
        RoundedImageView video;
        @BindView(R.id.video_play)
        ImageView videoPlay;
        @BindView(R.id.video_layout)
        RelativeLayout videoLayout;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
