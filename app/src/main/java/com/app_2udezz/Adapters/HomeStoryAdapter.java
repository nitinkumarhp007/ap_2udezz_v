package com.app_2udezz.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Activities.SingleStoryViewActivity;
import com.app_2udezz.Activities.StoryViewActivity;
import com.app_2udezz.DataModels.PostModel;
import com.app_2udezz.Fragments.HomeFragment;
import com.app_2udezz.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class HomeStoryAdapter extends RecyclerView.Adapter<HomeStoryAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    ArrayList<PostModel> list;
    private View view;
    HomeFragment homeFragment;

    public HomeStoryAdapter(Context context, ArrayList<PostModel> list, HomeFragment homeFragment) {
        this.context = context;
        this.list = list;
        this.homeFragment = homeFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.home_story_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.name.setText(list.get(position).getName());
        Glide.with(context).load(list.get(position).getImage()).into(holder.image);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SingleStoryViewActivity.class);
                intent.putExtra("list", list);
                intent.putExtra("position", position);
                intent.putExtra("is_friend", true);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progress_bar)
        ProgressBar progressBar;
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name)
        TextView name;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
