package com.app_2udezz.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Activities.ChattngActivity;
import com.app_2udezz.Activities.CommentsActivity;
import com.app_2udezz.Activities.FriendProfileActivity;
import com.app_2udezz.DataModels.ChattingModel;
import com.app_2udezz.DataModels.UserModel;
import com.app_2udezz.Fragments.ChatFragment;
import com.app_2udezz.R;
import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MentionUserAdapter extends RecyclerView.Adapter<MentionUserAdapter.RecyclerViewHolder> {
    CommentsActivity context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<UserModel> list;
    ArrayList<UserModel> tempList;

    public MentionUserAdapter(CommentsActivity context, ArrayList<UserModel> list) {
        this.context = context;
        this.list = list;
        this.tempList = list;
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.mention_user_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        holder.name.setText(list.get(position).getFirst_name());
        Glide.with(context).load(list.get(position).getProfile()).into(holder.profilePic);


        if (list.get(position).getVerfiy_badge().equals("1")) {
            holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.verified, 0);

        } else {
            holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.setclickdata(list.get(position).getFirst_name());
            }
        });
        holder.profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.setclickdata(list.get(position).getFirst_name());
            }
        });
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.setclickdata(list.get(position).getFirst_name());
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_pic)
        ImageView profilePic;
        @BindView(R.id.name)
        TextView name;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<UserModel> nList = new ArrayList<UserModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (UserModel wp : tempList) {
                if ((wp.getFirst_name().toLowerCase().contains(charText.toLowerCase())))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }


}
