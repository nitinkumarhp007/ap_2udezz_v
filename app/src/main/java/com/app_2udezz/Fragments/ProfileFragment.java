package com.app_2udezz.Fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Activities.LoginActivity;
import com.app_2udezz.Activities.SettingActivity;
import com.app_2udezz.Activities.UpdateProfileActivity;
import com.app_2udezz.Activities.UsersListActivity;
import com.app_2udezz.Adapters.ProfileAdapter;
import com.app_2udezz.DataModels.PostModel;
import com.app_2udezz.R;
import com.app_2udezz.Util.ConnectivityReceiver;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;
import com.app_2udezz.parser.GetAsyncDELETE;
import com.app_2udezz.parser.GetAsyncGet;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {


    Context context;
    @BindView(R.id.scroll_bar)
    ScrollView scrollBar;
    @BindView(R.id.error_text)
    TextView errorText;
    @BindView(R.id.main_layout)
    RelativeLayout mainLayout;
    @BindView(R.id.likes)
    TextView likes;
    @BindView(R.id.likes_layout)
    LinearLayout likesLayout;
    private SavePref savePref;
    Unbinder unbinder;
    @BindView(R.id.setting)
    ImageView setting;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email_address)
    TextView emailAddress;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.followers)
    TextView followers;
    @BindView(R.id.followers_layout)
    LinearLayout followersLayout;
    @BindView(R.id.following)
    TextView following;
    @BindView(R.id.following_layout)
    LinearLayout followingLayout;
    @BindView(R.id.edit_profile)
    TextView editProfile;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    private ArrayList<PostModel> list;
    ProfileAdapter adapter = null;
    String verfiy_badge = "";

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected()) {
            USER_DETAIL_API();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }


    private void USER_DETAIL_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.POST_ID, "200");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.USER_DETAIL + "/" + savePref.getID(), formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject data = jsonmainObject.getJSONObject("data");

                            JSONObject user_info = data.getJSONObject("user_info");
                            name.setText(user_info.getString("first_name"));
                            emailAddress.setText(user_info.getString("about_us"));
                            //  emailAddress.setText(savePref.getStringLatest("data"));
                            //phone.setText(user_info.getString("phone"));
                            Glide.with(context).load(user_info.getString("profile")).into(profilePic);

                            followers.setText(data.getJSONObject("otherinfo").getString("total_follower"));
                            following.setText(data.getJSONObject("otherinfo").getString("total_following"));
                            likes.setText(data.optString("total_likes"));
                            verfiy_badge = user_info.getString("verfiy_badge");
                            if (user_info.getString("verfiy_badge").equals("1")) {
                                name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.verified, 0);

                            } else {
                                name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            }

                            JSONArray body = data.getJSONArray("posts");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                PostModel postModel = new PostModel();
                                postModel.setCreated(object.getString("created"));
                                postModel.setDescription(object.getString("description"));
                                postModel.setId(object.getString("id"));
                                postModel.setIs_like(object.getString("is_like"));
                                postModel.setLocations(object.getString("locations"));
                                postModel.setMedia(object.getString("media"));
                                postModel.setPost_type(object.getString("post_type"));
                                postModel.setTitle(object.getString("title"));
                                postModel.setTotal_comments(object.getString("total_comments"));
                                postModel.setTotal_likes(object.getString("total_likes"));
                                postModel.setUser_id(object.getString("user_id"));
                                postModel.setVerfiy_badge(object.optString("verfiy_badge"));
                                postModel.setImage(user_info.getString("profile"));
                                postModel.setMedia_type(object.optString("media_type"));
                                postModel.setIs_show_delete(true);
                                postModel.setName(user_info.getString("first_name"));
                                list.add(postModel);
                            }
                            scrollBar.setVisibility(View.VISIBLE);
                            if (list.size() > 0) {
                                adapter = new ProfileAdapter(context, list, ProfileFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                myRecyclerView.setAdapter(adapter);

                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                            } else {
                                errorText.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }


                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                    savePref.clearPreferences();
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                } else {
                                    util.showToast(context, jsonmainObject.getString("error_message"));
                                }
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void DeleteAlert(int position) {
        new IOSDialog.Builder(context)
                .setCancelable(false)
                .setMessage("Are you sure to Delete Post?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DELETE_THREAD_API(position);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void DELETE_THREAD_API(int position) {
        util.hideKeyboard(getActivity());
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.POST_ID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncDELETE mAsync = new GetAsyncDELETE(context, AllAPIS.DELETE_POST + "?post_id=" + list.get(position).getId(), formBody, mDialog, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(Response response) {
                mDialog.dismiss();

                String result = response.body().toString();
                if (response.isSuccessful()) {
                    new IOSDialog.Builder(context)
                            .setCancelable(false)
                            .setMessage("Post Deleted Successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            list.remove(position);
                            adapter.notifyDataSetChanged();
                        }
                    }).show();
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void LIKE_DISLIKE_POST(int position) {
        String api_url = "";
        if (list.get(position).getIs_like().equals("1")) {
            api_url = AllAPIS.POST_DISLIKE;
        } else {
            api_url = AllAPIS.POST_LIKE;
        }

        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.POST_ID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, api_url, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, jsonMainobject.getString("message"));
                            if (list.get(position).getIs_like().equals("0")) {
                                list.get(position).setIs_like("1");
                                list.get(position).setTotal_likes(String.valueOf(Integer.parseInt(list.get(position).getTotal_likes()) + 1));
                                likes.setText(String.valueOf(Integer.parseInt(likes.getText().toString()) + 1));

                            } else {
                                list.get(position).setIs_like("0");
                                list.get(position).setTotal_likes(String.valueOf(Integer.parseInt(list.get(position).getTotal_likes()) - 1));
                                likes.setText(String.valueOf(Integer.parseInt(likes.getText().toString()) - 1));
                            }
                            adapter.notifyDataSetChanged();

                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.setting, R.id.followers_layout, R.id.following_layout, R.id.edit_profile})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.setting:
                Intent intent = new Intent(context, SettingActivity.class);
                intent.putExtra("verfiy_badge", verfiy_badge);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.followers_layout:
                GOTONEXT("1");
                break;
                case R.id.following_layout:
                GOTONEXT("0");
                break;
            case R.id.edit_profile:
                startActivity(new Intent(context, UpdateProfileActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void GOTONEXT(String type) {
        Intent intent = new Intent(context, UsersListActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("user_id", savePref.getID());
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
}
