package com.app_2udezz.Fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Activities.AddStoryActivity;
import com.app_2udezz.Activities.LoginActivity;
import com.app_2udezz.Activities.SingleStoryViewActivity;
import com.app_2udezz.Activities.StoryViewActivity;
import com.app_2udezz.Activities.TrendingActivity;
import com.app_2udezz.Adapters.HomeAdapter;
import com.app_2udezz.Adapters.HomeStoryAdapter;
import com.app_2udezz.Adapters.TrendingPostsAdapter;
import com.app_2udezz.DataModels.PostModel;
import com.app_2udezz.DataModels.StoryModel;
import com.app_2udezz.R;
import com.app_2udezz.Util.ConnectivityReceiver;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;
import com.app_2udezz.parser.GetAsyncGet;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    Context context;
    @BindView(R.id.search_bar)
    TextView searchBar;
    @BindView(R.id.scroll_bar)
    NestedScrollView scrollBar;
    @BindView(R.id.trending)
    TextView trending;
    @BindView(R.id.error_text)
    TextView errorText;
    @BindView(R.id.posts)
    TextView posts;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.my_story)
    RelativeLayout myStory;
    private SavePref savePref;
    Unbinder unbinder;
    @BindView(R.id.my_recycler_view_top)
    RecyclerView myRecyclerViewTop;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    HomeAdapter adapter = null;
    ProgressDialog mDialog;
    private ArrayList<PostModel> list = new ArrayList<>();
    private ArrayList<PostModel> list_trending;
    private ArrayList<PostModel> list_story;
    private ArrayList<PostModel> my_stories;
    TrendingPostsAdapter trendingPostsAdapter;
    private String offset = "1";
    private int total_post = 0;
    LinearLayoutManager linearLayoutManager;
    HomeStoryAdapter storyAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        util.story_pause = false;

        Glide.with(context).load(savePref.getImage()).into(image);

        mDialog = util.initializeProgress(context);

        linearLayoutManager = new LinearLayoutManager(context);
        myRecyclerView.setLayoutManager(linearLayoutManager);

        myRecyclerView.setNestedScrollingEnabled(false);


        scrollBar.getViewTreeObserver().addOnScrollChangedListener(() -> {
            View view11;
            if (scrollBar != null) {
                view11 = (View) scrollBar.getChildAt(scrollBar.getChildCount() - 1);

                Log.d("CategoryNeswList", scrollBar.getChildCount() + " child");
                int diff = (view11.getBottom() - (scrollBar.getHeight() + scrollBar
                        .getScrollY()));

                if (diff == 0) {
                    // LoadmoreElements();
                    if (list.size() == total_post) {
                        Toast.makeText(getActivity(), "No More Posts to Display", Toast.LENGTH_SHORT).show();
                    } else {
                        //Toast.makeText(context, "Loading more...", Toast.LENGTH_SHORT).show();
                        offset = String.valueOf(Integer.valueOf(offset) + 1);
                        POSTS();
                    }

                    //Toast.makeText(context, "Load More", Toast.LENGTH_SHORT).show();
                }
            }

        });


        //LoadmoreElements();


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected()) {
            offset = "1";
            POSTS();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void POSTS() {
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.POSTS + "/" + offset, formBody) {
            @Override
            public void getValueParse(String result) {
                if (list.size() > 0 && offset.equals("1")) {
                    list = new ArrayList<>();
                    list.clear();
                }
                if (offset.equals("1")) {
                    MY_STORIES();
                    //mDialog.dismiss();
                } else {
                    mDialog.dismiss();
                }
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONObject("data").getJSONArray("result");

                            total_post = Integer.parseInt(jsonmainObject.getJSONObject("data").getJSONObject("pagination").getString("totalRecord"));

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                PostModel postModel = new PostModel();
                                postModel.setCreated(object.getString("created"));
                                postModel.setDescription(object.getString("description"));
                                postModel.setId(object.getString("id"));
                                postModel.setIs_like(object.getString("is_like"));
                                postModel.setLocations(object.getString("locations"));
                                postModel.setMedia(object.getString("media"));
                                postModel.setPost_type(object.getString("post_type"));
                                postModel.setTitle(object.getString("title"));
                                postModel.setTotal_comments(object.getString("total_comments"));
                                postModel.setTotal_likes(object.getString("total_likes"));
                                postModel.setUser_id(object.getString("user_id"));
                                postModel.setVerfiy_badge(object.optString("verfiy_badge"));

                                if (object.getString("user_id").equals(savePref.getID())) {
                                    postModel.setIs_show_delete(true);
                                } else {
                                    postModel.setIs_show_delete(false);
                                }


                                postModel.setMedia_type(object.optString("media_type"));

                                postModel.setImage(object.getString("profile"));
                                postModel.setName(object.getString("first_name"));
                                list.add(postModel);
                            }

                            if (list.size() > 0) {

                                posts.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);

                                if (Integer.valueOf(offset) > 1)
                                    myRecyclerView.scrollToPosition(linearLayoutManager.findLastCompletelyVisibleItemPosition());

                                adapter = new HomeAdapter(context, list, HomeFragment.this);
                                myRecyclerView.setAdapter(adapter);
                            } else {
                                errorText.setVisibility(View.VISIBLE);
                                posts.setVisibility(View.GONE);
                                myRecyclerView.setVisibility(View.GONE);
                            }

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void LoadmoreElements() {
        myRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) //check for scroll down
                {
                    if (linearLayoutManager.findLastCompletelyVisibleItemPosition()
                            == list.size() - 1) {
                        if (list.size() == total_post) {
                            Toast.makeText(getActivity(), "No More Posts to Display", Toast.LENGTH_SHORT).show();
                        } else {
                            //Toast.makeText(context, "Loading more...", Toast.LENGTH_SHORT).show();
                            offset = String.valueOf(Integer.valueOf(offset) + 1);
                            POSTS();
                        }
                    }

                }
            }
        });

    }

    private void MY_STORIES() {
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.ALL_STORIES, formBody) {
            @Override
            public void getValueParse(String result) {
                list_story = new ArrayList<>();
                my_stories = new ArrayList<>();
                try {
                    mDialog.dismiss();
                } catch (Exception e) {
                }

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray myStories = jsonmainObject.getJSONObject("data").getJSONArray("myStories");

                            if (myStories.length() > 0) {
                                PostModel postModel = new PostModel();
                                postModel.setImage(savePref.getImage());
                                postModel.setName(savePref.getName());
                                postModel.setUser_id(savePref.getID());

                                ArrayList<StoryModel> list_story_inner = new ArrayList<>();

                                for (int i = 0; i < myStories.length(); i++) {
                                    JSONObject object = myStories.getJSONObject(i);
                                    StoryModel storyModel = new StoryModel();
                                    storyModel.setId(object.getString("id"));
                                    storyModel.setPost(object.getString("post"));
                                    storyModel.setPost_type(object.getString("post_type"));
                                    storyModel.setUser_id(object.getString("user_id"));
                                    storyModel.setImage(savePref.getImage());
                                    storyModel.setName(savePref.getName());
                                    storyModel.setThumbImage(object.optString("thumbImage"));
                                    storyModel.setTotalViews(object.optString("totalViews"));
                                    list_story_inner.add(storyModel);
                                }
                                postModel.setList(list_story_inner);
                                my_stories.add(postModel);
                            }


                            if (my_stories.size() > 0) {
                                name.setText("Your Story");
                            } else {
                                name.setText("Add Story");
                            }

                            JSONArray friendStories = jsonmainObject.getJSONObject("data").getJSONObject("firendStories").getJSONArray("result");
                            for (int i = 0; i < friendStories.length(); i++) {
                                JSONObject object = friendStories.getJSONObject(i);
                                PostModel postModel = new PostModel();
                                postModel.setImage(object.getString("profile"));
                                postModel.setName(object.getString("first_name"));


                                ArrayList<StoryModel> list = new ArrayList<>();
                                JSONArray stories = object.getJSONArray("stories");
                                for (int j = 0; j < stories.length(); j++) {
                                    JSONObject obj = stories.getJSONObject(j);
                                    StoryModel storyModel = new StoryModel();
                                    storyModel.setId(obj.getString("id"));
                                    storyModel.setPost(obj.getString("post"));
                                    postModel.setUser_id(obj.getString("user_id"));
                                    storyModel.setPost_type(obj.getString("post_type"));
                                    storyModel.setUser_id(obj.getString("user_id"));
                                    storyModel.setImage(object.getString("profile"));
                                    storyModel.setName(object.getString("first_name"));
                                    storyModel.setThumbImage(obj.optString("thumbImage"));
                                    storyModel.setTotalViews(obj.optString("totalViews"));
                                    list.add(storyModel);
                                }
                                postModel.setList(list);
                                list_story.add(postModel);

                            }

                            Collections.reverse(list_story);

                            if (list_story.size() > 0) {
                                //  trending.setVisibility(View.VISIBLE);
                                myRecyclerViewTop.setVisibility(View.VISIBLE);
                                storyAdapter = new HomeStoryAdapter(context, list_story, HomeFragment.this);
                                myRecyclerViewTop.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                myRecyclerViewTop.setAdapter(storyAdapter);

                            } else {
                                trending.setVisibility(View.GONE);
                                myRecyclerViewTop.setVisibility(View.GONE);
                            }

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void POSTS_TRENDING() {
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.POSTS_TRENDING, formBody) {
            @Override
            public void getValueParse(String result) {
                list_trending = new ArrayList<>();
                try {
                    mDialog.dismiss();
                } catch (Exception e) {
                }

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                PostModel postModel = new PostModel();
                                postModel.setCreated(object.getString("created"));
                                postModel.setDescription(object.getString("description"));
                                postModel.setId(object.getString("id"));
                                postModel.setIs_like(object.getString("is_like"));
                                postModel.setLocations(object.getString("locations"));
                                postModel.setMedia(object.getString("media"));
                                postModel.setPost_type(object.getString("post_type"));
                                postModel.setTitle(object.getString("title"));
                                postModel.setTotal_comments(object.getString("total_comments"));
                                postModel.setTotal_likes(object.getString("total_likes"));
                                postModel.setUser_id(object.getString("user_id"));
                                postModel.setVerfiy_badge(object.optString("verfiy_badge"));

                                postModel.setMedia_type(object.optString("media_type"));

                                postModel.setImage(object.getString("profile"));
                                postModel.setName(object.getString("first_name"));
                                list_trending.add(postModel);
                            }

                            if (list_trending.size() > 0) {
                                trending.setVisibility(View.VISIBLE);
                                myRecyclerViewTop.setVisibility(View.VISIBLE);
                                trendingPostsAdapter = new TrendingPostsAdapter(context, list_trending, HomeFragment.this);
                                myRecyclerViewTop.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                myRecyclerViewTop.setAdapter(trendingPostsAdapter);

                            } else {
                                trending.setVisibility(View.GONE);
                                myRecyclerViewTop.setVisibility(View.GONE);
                            }

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void LIKE_DISLIKE_POST(int position, boolean is_trending) {
        String api_url = "";
        if (list.get(position).getIs_like().equals("1")) {
            api_url = AllAPIS.POST_DISLIKE;
        } else {
            api_url = AllAPIS.POST_LIKE;
        }

        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (is_trending) {
            formBuilder.addFormDataPart(Parameters.POST_ID, list_trending.get(position).getId());
        } else {
            formBuilder.addFormDataPart(Parameters.POST_ID, list.get(position).getId());
        }
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, api_url, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, jsonMainobject.getString("message"));
                            if (is_trending) {
                                if (list_trending.get(position).getIs_like().equals("0")) {
                                    list_trending.get(position).setIs_like("1");
                                    list_trending.get(position).setTotal_likes(String.valueOf(Integer.parseInt(list_trending.get(position).getTotal_likes()) + 1));
                                } else {
                                    list_trending.get(position).setIs_like("0");
                                    list_trending.get(position).setTotal_likes(String.valueOf(Integer.parseInt(list_trending.get(position).getTotal_likes()) - 1));
                                }
                                trendingPostsAdapter.notifyDataSetChanged();
                            } else {
                                if (list.get(position).getIs_like().equals("0")) {
                                    list.get(position).setIs_like("1");
                                    list.get(position).setTotal_likes(String.valueOf(Integer.parseInt(list.get(position).getTotal_likes()) + 1));
                                } else {
                                    list.get(position).setIs_like("0");
                                    list.get(position).setTotal_likes(String.valueOf(Integer.parseInt(list.get(position).getTotal_likes()) - 1));
                                }
                                adapter.notifyDataSetChanged();
                            }


                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.search_bar, R.id.my_story})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.search_bar:
                startActivity(new Intent(getActivity(), TrendingActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.my_story:
                if (my_stories.size() > 0)
                    story_dialog();
                else {
                    startActivity(new Intent(getActivity(), AddStoryActivity.class));
                    getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }
                break;
        }
    }

    private void story_dialog() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        // builder.setTitle("Choose Seller Type");
        /*ArrayList to Array Conversion */
        String array[] = new String[2];
        array[0] = "Add Story";
        array[1] = "View My Stories";


        builder.setItems(array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    startActivity(new Intent(getActivity(), AddStoryActivity.class));
                    getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                } else {
                    Intent intent = new Intent(context, SingleStoryViewActivity.class);
                    intent.putExtra("list", my_stories);
                    intent.putExtra("position", 0);
                    intent.putExtra("is_friend", false);
                    context.startActivity(intent);
                }
            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
