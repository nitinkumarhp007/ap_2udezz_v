package com.app_2udezz.Fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app_2udezz.Activities.LoginActivity;
import com.app_2udezz.Adapters.PostMentionUserAdapter;
import com.app_2udezz.DataModels.ChattingModel;
import com.app_2udezz.DataModels.UserModel;
import com.app_2udezz.MainActivity;
import com.app_2udezz.R;
import com.app_2udezz.Util.ConnectivityReceiver;
import com.app_2udezz.Util.Parameters;
import com.app_2udezz.Util.SavePref;
import com.app_2udezz.Util.util;
import com.app_2udezz.parser.AllAPIS;
import com.app_2udezz.parser.GetAsync;
import com.app_2udezz.parser.GetAsyncGet;
import com.appunite.appunitevideoplayer.PlayerActivity;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddFragment extends Fragment {
    Context context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.topPanel)
    LinearLayout topPanel;
    private SavePref savePref;

    Unbinder unbinder;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.video_play)
    ImageView videoPlay;
    @BindView(R.id.title)
    EditText title;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.post_button)
    Button postButton;
    private ArrayList<UserModel> user_list;
    boolean is_image = false;
    String text_inputted = "";
    private final int PLACE_PICKER_REQUEST = 420;
    PostMentionUserAdapter mentionUserAdapter;
    String thumb1 = "", latitude = "", longitude = "", selectedimage = "";

    public AddFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        is_image = getArguments().getBoolean("is_image", false);
        selectedimage = "";
        selectedimage = getArguments().getString("filepath");
        if (is_image) {
            Glide.with(context).load(selectedimage).into(image);
        } else {
            Bitmap video_bitmap = ThumbnailUtils.createVideoThumbnail(selectedimage, MediaStore.Video.Thumbnails.MINI_KIND);
            thumb1 = getInsertedPath(video_bitmap, "");
            image.setImageBitmap(video_bitmap);
            // Glide.with(context).load(thumb1).into(image);
            videoPlay.setImageDrawable(getResources().getDrawable(R.drawable.video_play));
        }


        title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
              /*  if (s.toString().length() > 1) {
                    if (s.toString().substring(s.toString().length() - 1).equals("@")) {
                        Toast.makeText(context, "Open mention list", Toast.LENGTH_SHORT).show();
                    }
                }*/
              if(user_list!=null)
              {
                  if (user_list.size() > 0) {

                      int counter = 0;
                      if (s.toString().length() > 0) {
                          if (s.toString().length() == 1 && String.valueOf(s.toString().charAt(s.toString().length() - 1)).equals("@")) {
                             // Toast.makeText(context, "Open mention list", Toast.LENGTH_SHORT).show();
                              text_inputted = s.toString();
                              setAdapter1();
                          } else if (s.toString().length() > 1) {

                              if (String.valueOf(s.toString().charAt(s.toString().length() - 1)).equals("@") && String.valueOf(s.toString().charAt(s.toString().length() - 2)).equals(" ")) {
                                  //Toast.makeText(context, "Open mention list", Toast.LENGTH_SHORT).show();
                                  text_inputted = s.toString();
                                  setAdapter1();

                              }

                              for (int i = 0; i < s.toString().length(); i++) {
                                  if (s.charAt(i) == '@') {
                                      counter++;
                                      Log.e("Gone_list:", "3");
                                  }
                              }
                              Log.e("counter", String.valueOf(counter));
                              if (counter > 0) {
                                  if (mentionUserAdapter != null)
                                      mentionUserAdapter.filter(title.getText().toString().substring(s.toString().lastIndexOf("@") + 1, title.getText().toString().length()));
                              } else {
                                  topPanel.setVisibility(View.VISIBLE);
                                  myRecyclerView.setVisibility(View.GONE);
                              }
                          }
                      } else {
                          topPanel.setVisibility(View.VISIBLE);
                          myRecyclerView.setVisibility(View.GONE);
                      }
                  }
              }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (ConnectivityReceiver.isConnected()) {
            USER_LISTING();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }


        return view;
    }

    private void USER_LISTING() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, savePref.getID());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.FOLLOWING + "?follower=0&limit=100000", formBody) {
            @Override
            public void getValueParse(String result) {
                user_list = new ArrayList<>();
                if (user_list.size() > 0)
                    user_list.clear();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONObject("data").getJSONArray("result");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                UserModel userModel = new UserModel();
                                userModel.setEmail(object.getString("email"));
                                userModel.setFirst_name(object.getString("first_name"));
                                userModel.setLast_name(object.getString("last_name"));
                                userModel.setId(object.getString("id"));
                                userModel.setVerfiy_badge(object.optString("verfiy_badge"));
                                userModel.setProfile(object.getString("profile"));
                                user_list.add(userModel);
                            }


                        } else {
                            util.showToast(context, jsonmainObject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void setAdapter1() {
        myRecyclerView.setVisibility(View.VISIBLE);
        topPanel.setVisibility(View.GONE);
        mentionUserAdapter = new PostMentionUserAdapter(context, user_list, AddFragment.this);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(mentionUserAdapter);

    }

    public void setclickdata(String text) {
        util.hideKeyboard(getActivity());
        String text_inputted_new = text_inputted.substring(0, text_inputted.lastIndexOf("@"));
        title.setText(text_inputted_new + "@" + text.replace(" ", "") + " ");
        title.setSelection(title.getText().length());
        topPanel.setVisibility(View.VISIBLE);
        myRecyclerView.setVisibility(View.GONE);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.video_play, R.id.location, R.id.post_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.video_play:
                context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
                        getArguments().getString("filepath"),
                        " ", 0));
                break;
            case R.id.location:
                place_picker();
                break;
            case R.id.post_button:
                DoneTask();
                break;
        }
    }

    private void DoneTask() {
        if (ConnectivityReceiver.isConnected()) {
            if (title.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Title");
                postButton.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
            }/* else if (location.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Location");
                postButton.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
            } */ else {
                POST_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void POST_API() {
        util.hideKeyboard(getActivity());
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);


        if (!is_image) {
            if (!selectedimage.isEmpty()) {
                MediaType MEDIA_TYPE = null;
                MEDIA_TYPE = selectedimage.endsWith("mp4") ? MediaType.parse("/mp4") : MediaType.parse("/mp4");
                File file = new File(selectedimage);
                formBuilder.addFormDataPart(Parameters.MEDIA, file.getName(), RequestBody.create(MEDIA_TYPE, file));
            }

            if (!thumb1.isEmpty()) {
                final MediaType MEDIA_TYPE = thumb1.endsWith("png") ?
                        MediaType.parse("image/png") : MediaType.parse("image/jpeg");

                File file = new File(thumb1);
                formBuilder.addFormDataPart(Parameters.THUMB_IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
            }
            formBuilder.addFormDataPart(Parameters.MEDIA_TYPE, "2");
        } else {
            if (!selectedimage.isEmpty()) {
                final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                        MediaType.parse("image/png") : MediaType.parse("image/jpeg");

                File file = new File(selectedimage);
                formBuilder.addFormDataPart(Parameters.MEDIA, file.getName(), RequestBody.create(MEDIA_TYPE, file));
            }
            formBuilder.addFormDataPart(Parameters.MEDIA_TYPE, "1");
        }
        formBuilder.addFormDataPart(Parameters.TITLE, title.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, "descrption");
        formBuilder.addFormDataPart(Parameters.LOCATIONS, location.getText().toString());
        formBuilder.addFormDataPart(Parameters.LATITUDE, latitude);
        formBuilder.addFormDataPart(Parameters.LONGITUDE, longitude);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.POSTS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage("Post Added successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(context, MainActivity.class);
                                    intent.putExtra("from_add", true);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                }
                            }).show();
                        } else {
                            if (jsonObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void place_picker() {
        // Initialize Places.
        Places.initialize(context, "AIzaSyAA-k3KpN8PbS5u4_9qLFGIFZ_fIm52iM4");
        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(context);
        // Set the fields to specify which types of place data to return.
        List<com.google.android.libraries.places.api.model.Place.Field> fields = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.ID,
                com.google.android.libraries.places.api.model.Place.Field.NAME, com.google.android.libraries.places.api.model.Place.Field.LAT_LNG, com.google.android.libraries.places.api.model.Place.Field.ADDRESS, com.google.android.libraries.places.api.model.Place.Field.ID, com.google.android.libraries.places.api.model.Place.Field.PHONE_NUMBER, com.google.android.libraries.places.api.model.Place.Field.RATING, com.google.android.libraries.places.api.model.Place.Field.WEBSITE_URI);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields).build(context);

        startActivityForResult(intent, PLACE_PICKER_REQUEST);
    }

    public String getInsertedPath(Bitmap bitmap, String path) {
        String strMyImagePath = null;
        File mFolder = new File(Environment.getExternalStoragePublicDirectory("Ceedpage"), "");

        if (!mFolder.exists()) {
            mFolder.mkdir();
        }

        String s = String.valueOf(System.currentTimeMillis() / 1000) + ".jpg";
        File f = new File(mFolder.getAbsolutePath(), s);
        Log.e("creating", "name " + f.getName());
        strMyImagePath = f.getAbsolutePath();
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (strMyImagePath == null) {
            return path;
        }
        return strMyImagePath;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == PLACE_PICKER_REQUEST) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                String placeName = String.valueOf(place.getName());
                location.setText(placeName);
                latitude = String.valueOf(place.getLatLng().latitude);
                longitude = String.valueOf(place.getLatLng().longitude);
            }
        }
    }
}
